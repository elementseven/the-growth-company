@php
$page = 'About';
$pagetitle = 'About - The Growth Company';
$metadescription = "We are The Growth Company, advisors and experts on social selling and LinkedIn for business. We partner with you to scale your business for the digital buyer.";
$pagetype = 'dark';
$pagename = 'about';
$ogimage = 'https://thegrowthcompany.ie/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5 mob-pb-0 mt-5 mob-mt-0">
	<div class="row mt-5 pt-5">
		<div class="col-12 text-center text-lg-left">
		    <div class="position-relative z-2">
				<h1>About</h1>
				<div class="line line-primary my-3 text-left"><span class="ml-0 mob-mx-auto"></span></div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container">
	<div class="row text-center text-lg-left">
		<div class="col-lg-10">
			<p class="text-large">The Growth Company is Ireland’s leading LinkedIn Training and Consulting company, established by experienced social selling consultant and trainer Martin Murtagh in 2020 amidst a global pandemic.</p>
			<p class="text-large">The arrival of the Covid-19 virus forced businesses worldwide to review how they do business, and those slower to embrace digital in recent years quickly discovered that it was essential to their survival. </p>
			<p class="text-large">Similarly, the pandemic challenged Martin to review his long-term growth plans and approach to service provision. As a result, Martin’s business model became a hybrid mix of online and face-to-face coaching, mentoring, and training.</p>
			<p class="text-large">Before launching The Growth Company, Martin worked with many great clients throughout the UK and Ireland as a LinkedIn marketing consultant and trainer.</p>
			<p class="text-large">During this time, he built his reputation as a forward-thinking social selling expert whose strategies helped businesses strengthen relationships with their customers online.  </p>
			<p class="text-large mb-5">After much soul searching, Martin decided to take the next step and set up a new social selling  agency.  The Growth Company was therefore born!</p>
			<picture>
		        <source srcset="/img/about/martin-murtagh.webp" type="image/webp"/> 
		        <source srcset="/img/about/martin-murtagh.jpg" type="image/jpeg"/> 
		        <img src="/img/about/martin-murtagh.jpg" type="image/jpeg" alt="Martin Murtagh - The Growth Company" width="920" height="483" class="lazy h-auto w-100"/>
		    </picture>
			<p class="text-large mt-5">Our mission is to help Irish businesses  and beyond embrace digital transformation, not just to survive but to grow and thrive in a post-pandemic marketplace. </p> 
			<p class="text-large">One aspect of this journey that we’re particularly passionate about and recognised for is training and coaching people to get the best out of LinkedIn and <a href="/services/sales-navigator-training" class="text-primary">LinkedIn Sales Navigator</a>.</p>
			<p class="text-large">Many businesses work with us because of our LinkedIn expertise, and while we excel in this area, our services and overall value proposition is much broader.</p>
			<p class="text-large">Through our team of in-house experts and skilled associates, we also offer content strategy, content creation, as well managed social media services and thought leadership marketing. </p>
		</div>
		<div class="col-12 mt-5 pt-5">
			<p class="mimic-h3 mb-4">Who we work with:</p>
		</div>
		<client-logos></client-logos>
	</div>
</div>
<div class="container-fluid position-relative z-2 py-5">
    <picture>
        <source srcset="/img/graphics/globe-2.webp" type="image/webp"/> 
        <source srcset="/img/graphics/globe-2.png" type="image/png"/> 
        <img src="/img/graphics/globe-2.png" type="image/png" alt="The Growth Company background globe 2" width="1170" height="619" class="lazy bg-left"/>
    </picture>
    <div class="row pt-5 mob-pt-0">
        <div class="col-12 text-center mb-5">
            <p class="mimic-h3 mb-5 pb-4">Hear from our clients…</p>
        </div>
        <testimonials></testimonials>
    </div>
</div>
<seen-enough title='Interested in <span class="text-primary">growing</span> your business?' sentence="Our strategies help businesses strengthen their relationships with their customers online." :link="'/contact'" btntext="Let’s Talk"></seen-enough>
@endsection