@php
$page = 'Terms and Conditions';
$pagetitle = 'Terms and Conditions - The Growth Company';
$metadescription = "Read our terms and conditions";
$pagetype = 'dark';
$pagename = 'terms-and-conditions';
$ogimage = 'https://thegrowthcompany.ie/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5 mob-pb-0 mt-5 mob-px-4">
	<div class="row mt-5 pt-5 mob-pt-0">
		<div class="col-12 text-left">
		    <div class="position-relative z-2">
				<h1>Terms and Conditions</h1>
				<div class="line line-primary my-4 text-left"><span class="ml-0 "></span></div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
	<div class="row">
		<div class="col-lg-10">
			<p class="title text-larger">THESE TERMS</p>
			
			<p><span>1.1 What do these terms cover? These are the terms and conditions on which we supply our services (the &ldquo;Services&rdquo;) to the customer (the &ldquo;Customer&rdquo;).</span></p>
			
			<p><span>1.2 The Services. The Services include the provision of; (i) advice and (ii) mentoring/coaching, and (iii) social media managed services. In addition, we offer a range of different packages to suit the customer&rsquo;s needs which include; (i) Bronze, (ii) Silver, and (iii) Gold.</span></p>
			
			<p><span>1.3 Why you should read them? Please read these terms carefully before deciding if you&rsquo;d like to do business with us. These terms set out who we are, how we will provide our Services, how the customer and we may change or end the contract, what to do if there is a problem and other important information.</span></p>
			
			<p class="title text-larger mt-5 pt-5">INFORMATION ABOUT US AND HOW TO CONTACT US</p>
			
			<p><span>2.1 Who we are. We are The Growth Company (NI) Ltd, a company registered in Northern Ireland Ireland under company number NI679483, whose registered address is Hilden, 27 Windsor Hill, Newry, BT34 1ER.&nbsp;</span></p>
			
			<p><span>2.2 How to contact us. You can contact us by writing to us at hello@thegrowthcompany.ie</span></p>
			
			<p><span>2.3 How we may contact the customer. If we have to contact you, we will do so by telephone or emailing at the email address or postal address you provided.</span></p>
			
			<p class="title text-larger mt-5 pt-5">OUR CONTRACT WITH THE CUSTOMER</p>
			
			<p><span>3.1 How we will accept your Order. Agreeing to a relevant service or social media package will only occur when the customer agrees with a written proposal sent to us by email. We then email the customer to accept the Order, at which point a contract will come into existence between the customer and us (the &ldquo;Contract&rdquo;). The customer must ensure that the terms of the Order are complete and accurate.</span></p>
			
			<p><span>3.2 These Conditions apply to the contract to exclude any other terms that the customer seeks to impose or incorporate, implied by law, trade custom, practice or course of dealing.</span></p>
			
			<p><span>3.3 Any quotation given by The Growth Company shall not constitute an offer and is only valid for 20 business days from its issue date.</span></p>
			
			<p class="title text-larger mt-5 pt-5">SUPPLY OF SERVICES</p>
			
			<p><span>4.1 The Growth Company will supply the Services to the customer by the relevant package purchased in all material respects. The details of each package may vary from time to time.</span></p>
			
			<p><span>4.2 The Customer shall co-operate with The Growth Company in all matters relating to the provision of the Services.</span></p>
			
			<p><span>4.3 The Growth Company will use all reasonable endeavours to meet any performance dates agreed between the parties concerning the provision of the Services. Still, any such dates shall be estimates only, and time shall not be of the essence for the performance of the Services.</span></p>
			
			<p><span>4.4 The Growth Company reserves the right to amend the packages specification if necessary to comply with any applicable law or regulatory requirement or if the amendment will not materially affect the nature or quality of the Services. The Growth Company shall notify the customer in any such event.</span></p>
			
			<p class="title text-larger mt-5 pt-5">YOUR RIGHT TO MAKE CHANGES</p>
			
			<p><span>5.1 If the customer wishes to change the package they have ordered, please get in touch with us. We will let you know if the change is possible. If possible, we will inform the customer about any changes to the price of the Services, the timing of delivery, or anything else that would be necessary as a result of the requested change and ask the customer to confirm whether they wish to go ahead with the change. If we cannot make the change or the consequences of making the change are unacceptable, the customer may want to end the contract (see clause 9 &ndash; The Customer&rsquo;s rights to end the contract).</span></p>
			
			<p class="title text-larger mt-5 pt-5">OUR RIGHTS TO MAKE CHANGES</p>
			
			<p><span>Minor changes to the Services. We may change the Services provided:</span></p>
			
			<p><span>6.1 to reflect changes in relevant laws and regulatory requirements; and</span></p>
			
			<p><span>6.2 More significant changes to the Services and these terms. In addition, as we informed the customer in the description of the Services on our website, we may make the following changes to the packages. Still, if we do so, we will notify the customer, and the customer may then contact us to end the contract before the changes take effect and receive a refund for any Services paid for but not received:</span></p>
			
			<p><span>6.2.1 any substantial updates to the Services or the software;</span></p>
			
			<p><span>6.2.2 any new functionality provisions in the Services or the software.</span></p>
			
			<p><span>6.3 Updates to digital content. We may update or require the customer to update digital content. The digital content shall always match the description that we provided to the customer before the customer bought it.</span></p>
			
			<p class="title text-larger mt-5 pt-5">PROVIDING THE SERVICES</p>
			
			<p><span>7.1 Depending on the nature of the package purchased from us, we will provide the Services as follows:</span></p>
			
			<p><span>7.1.1 Ongoing services or a subscription to receive goods or digital content. We will supply the Services, goods or digital content to the customer until either (i) the Services are completed or (ii) the subscription expires (if applicable) or the customer ends the contract as described in clause 8 or we end the contract by written notice to the customer as described in clause 11.</span></p>
			
			<p><span>7.2 We are not responsible for delays outside our control. If an event outside our control delays our supply of the Services, we will contact the customer as soon as possible to let the customer know, and we will take steps to minimise the effect of the delay. Provided we do this, we will not be liable for delays caused by the event, but if there is a risk of substantial delay, the customer may contact us to end the contract and receive a refund for any Services the Customer have paid for but not received.</span></p>
			
			<p><span>7.5 What will happen if the customer does not give the required information to us. We may need certain information or documentation from the customer to supply the Services, for example, access to social media accounts. We will contact the customer to ask for this information.&nbsp;</span></p>
			
			<p><span>If the customer does not give us this within a reasonable time, or if incomplete or incorrect information is provided, we may either end the contract (and clause 11.2 will apply) or make an additional charge of a reasonable sum to compensate us for any extra work that is required as a result.&nbsp;</span></p>
			
			<p><span>We will not be responsible for supplying the Services late or not supplying any part of them if this is caused by the customer not giving us the information we need within a reasonable time of us asking for it. To avoid doubt, we will issue no refund to the customer, but the customer requested but not provided the required information.</span></p>
			
			<p><span>Reasons we may suspend the supply of Services to the customer. For example, we may have to stop the supply of the Services to:</span></p>
			
			<p><span>7.6.1 deal with technical problems or make minor technical changes;</span></p>
			
			<p><span>7.6.2 update the Services to reflect changes in relevant laws and regulatory requirements;</span></p>
			
			<p><span>7.6.3 make changes to the Services as requested by the customer or notified by us (see clause 6).</span></p>
			
			<p><span>7.7 Your rights if we suspend the supply of Services. We will contact the customer in advance to inform them that we will be suspending the supply of the Services unless the problem is urgent or an emergency. If we have to stop the Services, we will adjust the price to not pay for Services while suspended.&nbsp;</span></p>

		</div>
	</div>
</div>

<seen-enough title='Interested in <span class="text-primary">growing</span> your business?' sentence="Our strategies help businesses strengthen their relationships with their customers online." :link="'/contact'" btntext="Let’s Talk"></seen-enough>
@endsection