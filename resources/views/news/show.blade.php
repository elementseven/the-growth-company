@php
$page = 'News';
$pagetitle = $post->title . ' | The Growth Company';
$metadescription = $post->meta_description;
$keywords = $post->keywords;
$pagetype = 'dark';
$pagename = 'news';
$ogimage = 'https://thegrowthcompany.ie' . $post->getFirstMediaUrl('double');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
<header class="container-fluid position-relative bg bg-down-up z-1 menu-padding mb-5 mob-mb-0">
  <div class="row mt-5 pt-5">
    <div class="container">
      <div class="row justify-content-center pt-5">
        <div class="col-12 pt-5 ipadp-pt-0 mob-pt-0 mob-mt-0 text-center text-lg-left mob-mb-3">
          <p class=""><a href="{{route('news.index')}}" class=" text-primary"><i class="fa fa-angle-double-left mr-2"></i><b>Browse all news</b></a></p>
          <h1 class="mb-3 blog-title">{{$post->title}}</h1>
          <p class="text-large mb-4">{{$post->excerpt}}</p>
          <p class="mb-1"><b>Share this article:</b></p>
          <p class="text-larger">
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-primary">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-primary">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-primary">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-primary">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
      </div>
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
    <div class="row pb-5 mob-py-0 text-left">
        <div class="col-12 mob-mt-0 mb-5 mob-mb-3">
          <div class="backdrop overflow-hidden pb-3">
            <picture>
              <source media="(min-width: 1200px)" srcset="{{$post->getFirstMediaUrl('blogs','double-webp')}}" type="image/webp"/> 
              <source media="(min-width: 1200px)" srcset="{{$post->getFirstMediaUrl('blogs','double')}}" type="{{$post->getFirstMedia('blogs')->mime_type}}"/> 
              <source media="(min-width: 768px)" srcset="{{$post->getFirstMediaUrl('blogs','normal-webp')}}" type="image/webp"/> 
              <source media="(min-width: 768px)" srcset="{{$post->getFirstMediaUrl('blogs','normal')}}" type="{{$post->getFirstMedia('blogs')->mime_type}}"/> 
              <source media="(min-width: 1px)" srcset="{{$post->getFirstMediaUrl('blogs','featured-webp')}}" type="image/webp"/> 
              <source media="(min-width: 1px)" srcset="{{$post->getFirstMediaUrl('blogs','featured')}}" type="{{$post->getFirstMedia('blogs')->mime_type}}"/> 
              <img src="{{$post->getFirstMediaUrl('blogs','featured')}} 1w,  {{$post->getFirstMediaUrl('blogs','normal')}} 768w, {{$post->getFirstMediaUrl('blogs','double')}} 1200w" type="{{$post->getFirstMedia('blogs')->mime_type}}" alt="{{$post->title}}" class="w-100 backdrop-content lazy" />
            </picture>
            <div class="backdrop-back" data-aos="fade-down-right"></div>
          </div>
        </div>
        <div class="col-12 mob-mt-0 blog-body">
          {!!$post->content!!}
        </div>
        <div class="col-12 mt-5 ">
          <p class="mb-1"><b>Share this article:</b></p>
          <p class="text-larger">
              <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-primary">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-primary">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-primary">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-primary">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
    </div>
</div>
<seen-enough title='More blog posts from The Growth Company' :link="'{{route('news.index')}}'" :btntext="'See All Blog Posts'"></seen-enough>
@endsection
@section('scripts')
<script>
    $(window).load(function(){
        $('.ql-video').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
        $('.fr-video iframe').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
    });
</script>
@endsection