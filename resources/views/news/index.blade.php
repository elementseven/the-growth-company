@php
$page = 'News';
$pagetitle = 'Latest News - The Growth Company';
$metadescription = 'We are The Growth Company, advisors and experts on social selling and LinkedIn for business.';
$pagetype = 'dark';
$pagename = 'news';
$ogimage = 'https://thegrowthcompany.ie/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5 mob-pb-0 mt-5">
	<div class="row mt-5 pt-5 mob-pt-0">
		<div class="col-12 text-center text-lg-left">
			<h1>Blog</h1>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large">Check out our blog posts below:</p>
		</div>
	</div>
	

</header>
@endsection
@section('content')
<news-index :categories="{{$categories}}"></news-index>
<seen-enough title='Interested in <span class="text-primary">growing</span> your business?' sentence="Our strategies help businesses strengthen their relationships with their customers online." :link="'/contact'" btntext="Let’s Talk"></seen-enough>
@endsection