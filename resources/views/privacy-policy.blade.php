@php
$page = 'Privacy Policy';
$pagetitle = 'Privacy Policy - The Growth Company';
$metadescription = "Read our Privacy Policy";
$pagetype = 'dark';
$pagename = 'privacy-policy';
$ogimage = 'https://thegrowthcompany.ie/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5 mob-pb-0 mt-5 mob-px-4">
	<div class="row mt-5 pt-5 mob-pt-0">
		<div class="col-12 text-left">
		    <div class="position-relative z-2">
				<h1>Privacy Policy</h1>
				<div class="line line-primary my-4 text-left"><span class="ml-0 "></span></div>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
	<div class="row">
		<div class="col-lg-10">			

			<p><span>The Growth Company (NI) Ltd operate this website. Registered in Northern Ireland under company number NI679483, whose registered address is Hilden, 27 Windsor Hill, Newry, BT34 1ER.&nbsp;</span></p>
			
			<p><span>The following privacy policy relates to your use of our website: </span><a class="text-primary" href="https://www.thegrowthcompany.ie"><span>https://www.thegrowthcompany.ie</span></a></p>
			
			<p><span>This privacy policy aims to give you information on how The Growth Company (NI) Ltd collects and processes your data through your use of our website, including any data you may provide when you sign up to our newsletter or register for a service.</span></p>
			<p><strong>&nbsp;</strong></p>
			
			<p class="title text-larger mt-5">BACKGROUND</p>
			
			<p><strong>Data Controller</strong><span> The Growth Company (NI) Ltd is the controller and responsible for your datadata (referred to as "</span><strong>The Growth Company</strong><span>", ""</span><strong>we</strong><span>"", ""</span><strong>us</strong><span>"" or ""</span><strong>our</strong><span>"" in this privacy policy).</span></p>
			
			<p><span>If you have any questions about this privacy policy, including any requests to exercise your legal rights, please contact us using the details set out below.</span></p>
			<p><strong>&nbsp;</strong></p>
			<p><strong>Contact details</strong></p>
			
			<p><span>If you have any questions about this privacy policy or our privacy practices, please get in touch with us in the following ways:</span></p>
			
			<p><span>Email address: </span><a href="mailto:hello@thegrowthcompany.ie" class="text-primary"><span>hello@thegrowthcompany.ie</span></a></p>
			
			<p><span>Postal address: The Growth Company, Hilden, 27 Windsor Hill, Newry BT34 1ER</span></p>
			
			<p><span>Telephone number: <a href="tel:00447512685946" class="text-primary">+44 (0) 7512685946</a></span></p>
			
			<p><span>You have the right to make a complaint at any time to the Information Commissioner's Commissioner's Office (ICO), the UK supervisory authority for data protection issues (</span><a class="text-primary" href="http://www.ico.org.uk"><span>www.ico.org.uk</span></a><span>). We would, however, appreciate the chance to deal with your concerns before you approach the ICO, so don't hesitate to get in touch with us in the first instance.</span></p>
			<p><strong>&nbsp;</strong></p>
			<p><strong>Changes to the privacy policy and your duty to inform us of changes</strong></p>
			
			<p><span>We keep our privacy policy under regular review. This version was last updated on the date October 2021.</span></p>
			
			<p><span>The personal data we hold about you must be accurate and current.&nbsp;</span></p>
			
			<p><span>Please keep us informed if your data changes during your relationship with us.</span></p>
			<p><strong>&nbsp;</strong></p>
			<p><strong>Third-Party Links</strong></p>
			<p><span>We may link to other websites owned and operated by certain trusted third parties. These other third party websites may also gather information about you under separate privacy policies. For privacy information relating to these other third party websites, please consult their privacy policies as appropriate.</span></p>
			<p><strong>&nbsp;</strong></p>
			<p><strong>Customers using The Growth Company methodology, products &amp; services</strong></p>
			
			<p><span>The Growth Company provides methodologies, solutions &amp; services to its customers. If your organisation provides you with access to The Growth Company materials, we are likely acting as a data processor to your organisation. In these circumstances, you should contact your organisation directly about any data protection queries you have, including any requests to exercise your data protection rights.</span></p>
			<p><strong>&nbsp;</strong></p>
			
			<p class="title text-larger mt-5">DATA WE COLLECT ABOUT YOU</p>
			
			
			<p><span>Personal data, or personal information, means any information about an individual from which that person's identified. It does not include data where the identity has been removed (anonymous data).</span></p>
			
			<p><span>We may collect, use, store and transfer different kinds of personal data about you, which we have grouped as follows:</span></p>
			
			<ul>
			<li><strong>Identity Data</strong><span> includes first name, last name, username or similar identifier.</span></li>
			<li><strong>Contact Data</strong><span> includes email address and telephone number.</span></li>
			<li><strong>Technical Data</strong><span> includes (IP) address, your login data, browser type and version, time zone setting and location, browser plug-in types and versions, operating system and platform, and other technology on the devices you use to access our site.</span></li>
			<li><strong>Profile Data</strong><span> includes your username and password, actions completed by you on our site, including responses in questionnaires, quizzes or surveys, your interests, preferences and feedback.</span></li>
			<li><strong>Social Media Data</strong><span> includes social media account details such as username, number of followers, frequency of use or activity and profile information.</span></li>
			<li><strong>Usage Data</strong><span> includes information about how you use our Sites, products and services.</span></li>
			<li><strong>Marketing and Communications Data</strong><span> includes your preferences in receiving marketing from third parties and us and your communication preferences.</span></li>
			</ul>
			
			<p><span>We also collect, use and share aggregated data such as statistical or demographic data for any purpose. Aggregated Data could be derived from your data but is not considered personal data in law as this data will </span><strong>not</strong><span> directly or indirectly reveal your identity. For example, we may aggregate your Usage Data to calculate the percentage of users accessing a specific website feature or Social Media Data to produce reports for your employer. However, suppose we combine or connect aggregated data with your data to directly or indirectly identify you. In that case, we treat the combined data as personal data that we will use according to this privacy policy.</span></p>
			
			<p><span>We do not collect Data about you (this includes details about race or ethnicity, religious or philosophical beliefs, sex life, sexual orientation, political opinions, trade union membership, information about your health, and genetic and biometric data). Nor do we collect any information about criminal convictions and offences.</span></p>
			<p><strong>&nbsp;</strong></p>
			<p><strong>If you fail to provide personal data</strong></p>
			
			<p><span>Where we require data by law, or under the terms of a contract we have with you, and you fail to provide that data when requested, we may not be able to perform the contract we have or are trying to enter into with you (for example, to provide you with our services).&nbsp;</span></p>
			
			<p><span>In this case, we may have to cancel a service you have with us, but we will notify you at the time if this is the case.</span></p>
			<p><strong>&nbsp;</strong></p>
			
			<p class="title text-larger mt-5">WHEN DO WE COLLECT PERSONAL DATA?</p>
			
			
			<p><span>The information we collect depends on the context of your interactions with our site. Therefore, we use different methods to collect data from and about you including through:</span></p>
			
			<p><strong>Direct interactions. </strong><span>You may give us your Identity, Contact and Social Media Data by filling in forms or corresponding with us by post, phone, email or otherwise.</span></p>
			
			<p><span>This includes personal data you provide when you:</span></p>
			
			<ol>
			<li><span>register on, or browse our site;</span></li>
			<li><span>place an order for our services;</span></li>
			<li><span>create an account on our site;</span></li>
			<li><span>respond to a survey, quiz, questionnaire or fill out a form;</span></li>
			<li><span>use live chat or open a support ticket,</span></li>
			<li><span>subscribe to our newsletters or publications;</span></li>
			<li><span>request marketing sent to you; or</span></li>
			<li><span>Contact us by telephone, interact with us by email or social media, and provide feedback on our products or services.</span></li>
			</ol>
			<p><strong>&nbsp;</strong></p>
			<p class="title text-larger mt-5">Automated technologies or interactions.</p>
			
			<p><span>As you interact with our site, we will automatically collect Technical Data about your equipment, browsing actions and patterns. We collect this personal data by using cookies and other similar technologies. Please see our information on cookies in section 6 below for further details.</span></p>
			<p><strong>&nbsp;</strong></p>
			<p class="title text-larger mt-5">Third parties or publicly available sources.</p>
			
			<p><span>We may receive Technical Data from the following third parties:</span></p>
			
			<ol>
			<li><span>analytics providers;</span></li>
			<li><span>advertising networks; and</span></li>
			<li><span>search information providers.</span></li>
			</ol>

			<p class="title text-larger mt-5">How do we use your information?</p>
			
			<p><span>We will only use your data when the law allows us to. Most commonly, we may use the information we collect from you in the following circumstances:</span></p>
			
			<ul>
			<li><span>We need to perform the contract we are about to enter into or have entered into with you.</span></li>
			<li><span>Where it is necessary for our legitimate interests (or those of a third party), your interests and fundamental rights do not override those interests.</span></li>
			<li><span>When we need to comply with a legal obligation.</span></li>
			</ul>
			
			<p><span>We also use software, tools, or systems to make automated decisions (including profiling) based on the personal information we have or collect from others to profile you to understand your social media needs better and provide a better service. Please note that the section on Your Rights set out below applies to profiling.&nbsp;</span></p>
			
			<p><span>See the Glossary to learn more about the types of lawful basis that we will rely on to process your data.</span></p>
			
			<p><span>Generally, we do not rely on consent as a legal basis for processing your data. However, you have the right to withdraw consent to marketing at any time by contacting us.</span></p>
			
			<p><span>This website's not intended for use by children under the age of 13, and we do not knowingly collect or use personal information relating to children.</span></p>
			<p><strong>&nbsp;</strong></p>
			
			<p class="title text-larger mt-5">PURPOSES FOR WHICH WE WILL USE YOUR DATA</p>
			
			
			<p><span>We may use the personal data collected for the following purposes:</span></p>
			
			<ul>
			<li><span>To personalise your experience and allow us to deliver the type of content and product offerings that interest you.</span></li>
			<li><span>To improve our site to serve our customers better.&nbsp;</span></li>
			<li><span>To allow us to better service you in responding to customer service requests.</span></li>
			<li><span>For statistical analysis and customer profiling to assist us in providing and improving the services (e.g. to understand digital behaviours)</span></li>
			<li><span>To administer promotions, surveys or other site features.</span></li>
			<li><span>To send periodic emails regarding tips, news, orders, products and services.</span></li>
			<li><span>To follow up with you after correspondence.</span></li>
			<li><span>To verify your identity.</span></li>
			<li><span>To notify you of any changes to our site or to our services that may affect you.</span></li>
			</ul>
			
			<p class="title text-larger mt-5">Our legal basis for processing your personal information</p>
			
			<p><span>You may choose not to provide certain information, but if you do, and that information is necessary to provide a particular feature, you may not be able to use that feature. We will only use your data where we have a valid lawful basis to do so. Depending on what personal information we process and why there are various legal bases upon which we may rely.</span></p>
			
			<p class="title text-larger mt-5">DATA SECURITY</p>
			
			
			<p><strong>How do we protect your information?</strong></p>
			
			<p><span>Your personal information is contained behind secured networks and is only accessible by a limited number of persons who require access rights to such systems to perform their tasks. All persons with access are required to keep the information confidential.</span></p>
			
			<p><span>We also have procedures in place to deal with any suspected data security breach. We will notify you and any applicable regulator of a suspected data security breach where we are legally required to do so.</span></p>

			<p class="title text-larger mt-5">COOKIES</p>
			
			<p><span>If you agree, we may use cookies. Cookies are small files that a site or its service provider saves to your device through your Web browser (if you allow), enabling the site or service provider's systems to recognise your browser and capture and remember certain information. Details about the cookies we use can be found</span> <a class="text-primary" href="https://knowledge.hubspot.com/articles/kcs_article/reports/what-cookies-does-hubspot-set-in-a-visitor-s-browser?__hstc=146521643.5bf3842d8b4ca1f4812120e9b0b2fa2c.1633558922012.1633558922012.1633558922012.1&amp;__hssc=146521643.1.1633558922012&amp;__hsfp=2530750139"><strong>here</strong></a><strong>.</strong></p>
			
			<p><span>With your permission, we collect information about how you interact with our website. We use this information to improve your website experience, customise your browsing experience, and analytics and metrics about our visitors on this website and other media.</span></p>
			
			<p><span>You can choose to have your browser warn you each time a cookie is active, or you can turn off all cookies. You can do this through your browser settings. Since all browsers are a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.</span></p>
			
			<p><span>Suppose you turn cookies off or decline permission to use cookies. In that case, some of the features that make your site experience more efficient and personalised will be hidden or replaced with generic content.</span></p>
			
			<p><span>For further information on cookies, generally, visit </span><a class="text-primary" href="http://www.aboutcookies.org"><span>www.aboutcookies.org</span></a><span> or </span><a class="text-primary" href="http://www.allaboutcookies"><span>www.allaboutcookies</span></a><span>.org.</span></p>
			
			<p class="title text-larger mt-5">DISCLOSURE OF YOUR DATA</p>
			
			
			<p><strong>Third-party disclosure</strong></p>
			
			<p><span>We will not sell, trade, or otherwise transfer to outside parties your personal information. This excludes website hosting partners and other parties who assist us in operating our website, conducting our business, or serving our users. We require all third parties to agree to keep personal information confidential. We only use it to support The Growth Company Ltd. We may also share personal information with law enforcement or other authorities if applicable law requires.&nbsp;</span></p>
			
			<p><span>We may share personal information with third parties to whom we may choose to sell, transfer or merge parts of our business or our assets. Alternatively, we may seek to acquire other companies or merge with them. If a change happens to our business, the new owners may use your data the same way as set out in this privacy policy.</span></p>
			
			<p><span>We require all third parties to respect the security of your data and treat it according to the law. Therefore, we do not allow our third-party service providers to use your data for their purposes and only permit them to process your data for specified purposes and under our instructions.</span></p>


			<p class="title text-larger mt-5">Overseas Transfers</p>
			<p><span>We may transfer to and store the data we collect about you in countries other than the country in which we originally collected the data, including the United States or other destinations outside the European Economic Area (""EEA""). Those countries may not have the same data protection laws as the country where you provided the data.&nbsp;</span></p>
			
			<p><span>When we transfer your data to other countries, we will protect the data described in this privacy policy and comply with applicable legal requirements providing adequate protection for data transfer to countries outside the EEA.</span></p>
			
			<p><span>We will only transfer your data if:</span></p>
			<ul>
			<li><span>the country to which we will transfer the personal data has a European Commission adequacy decision;</span></li>
			<li><span>the recipient of the personal data is located in the US and has certified to the US-EU Privacy Shield Framework; or</span></li>
			<li><span>we have put in place appropriate safeguards in respect of the transfer, for example, we have entered into EU standard contractual clauses with the recipient, or the recipient is a party to binding corporate rules.</span></li>
			</ul>
			<p><span>You may request more information about the safeguards we have put in place regarding transfers of personal data by contacting us as described in the Contact Details section above.</span></p>
			
			<p><span>Whenever it is necessary to undertake such a transfer of your data, we ensure a similar degree of protection afforded to it by ensuring at least one of the following safeguards implemented:</span></p>
			
			<ul>
			<li><span>We will only transfer your data to countries deemed to provide an adequate level of protection for personal data by the European Commission. For further details, see </span><a class="text-primary" href="https://ec.europa.eu/info/law/law-topic/data-protection/data-transfers-outside-eu/adequacy-protection-personal-data-non-eu-countries_en"><em><span>European Commission: Adequacy of the protection of personal data in non-EU countries</span></em></a><em><span>.</span></em></li>
			<li><span>We use certain service providers, and we may use specific contracts approved by the European Commission, which give personal data the same protection it has in Europe. For further details, see </span><a class="text-primary" href="https://ec.europa.eu/info/strategy/justice-and-fundamental-rights/data-protection/data-transfers-outside-eu/model-contracts-transfer-personal-data-third-countries_en"><em><span>European Commission: Model contracts for the transfer of personal data to third countries</span></em></a><em><span>.</span></em></li>
			<li><span>We use providers based in the US, and we may transfer data to them if they are part of the Privacy Shield, which requires them to provide similar protection to personal data shared between Europe and the US. For further details, see </span><a class="text-primary" href="https://ec.europa.eu/info/strategy/justice-and-fundamental-rights/data-protection/data-transfers-outside-eu/eu-us-privacy-shield_en"><em><span>European Commission: EU-US Privacy Shield</span></em></a><em><span>.</span></em></li>
			</ul>
			
			<p><span>Please contact us if you want further information on the specific mechanism when transferring your data out of the EEA.</span></p>
			
			<p class="title text-larger mt-5">DATA RETENTION</p>
			
			<p><span>We will only retain your data for as long as reasonably necessary to fulfil the purposes we collected it for, including to satisfy any legal, regulatory, tax, accounting or reporting requirements. We may retain your data for a more extended period in the event of a complaint or if we reasonably believe there is a prospect of litigation regarding our relationship with you.</span></p>
			
			<p><span>To determine appropriate retention period for data, we consider the amount, nature and sensitivity of the data, the potential risk of harm from unauthorised or disclosure of your data, the purposes for which we process your data and whether we can achieve those purposes through other means, and the applicable legal, regulatory, tax, accounting or other requirements.</span></p>
			
			<p><span>Details of retention periods for different aspects of your data are available in our retention policy which you can request from us by contacting us at </span><a class="text-primary" href="mailto:hello@thegrowthcompany.ie"><span>hello@thegrowthcompany.ie&nbsp;</span></a></p>
			
			<p><span>In some circumstances, we will anonymise your data (so that it can no longer be associated with you) for research or statistical purposes, in which case we may use this information indefinitely without further notice to you.</span></p>
			
			<p class="title text-larger mt-5">YOUR LEGAL RIGHTS</p>
			
			<p><span>Under the General Data Protection Regulation, you have many necessary rights free of charge. In summary, those include rights to:</span></p>
			
			<ul>
			<li><span>fair processing of information and transparency over how we use your use personal information</span></li>
			<li><span>access to your personal information and to certain other supplementary information that this privacy policy addresses</span></li>
			<li><span>require us to correct any mistakes in your information which we hold</span></li>
			<li><span>require the erasure of personal information concerning you in certain situations</span></li>
			<li><span>receive the personal information concerning you which you have provided to us, in a structured, commonly used and machine-readable format and have the right to transmit those data to a third party in certain situations</span></li>
			<li><span>object at any time to processing of personal information concerning you for direct marketing and profiling</span></li>
			<li><span>request for a decision made by profiling to be reviewed by a human</span></li>
			<li><span>object to decisions taken by automated means, such as profiling, which produce legal effects concerning you or similarly significantly affect you</span></li>
			<li><span>object in certain other situations to our continued processing of your personal information</span></li>
			<li><span>otherwise, restrict our processing of your personal information in certain circumstances</span></li>
			</ul>
			
			<p><span>For further information on each of those rights, including the circumstances in which they apply, see the </span><a class="text-primary" href="https://ico.org.uk/for-organisations/guide-to-the-general-data-protection-regulation-gdpr/individual-rights/"><span>Guidance from the UK Information Commissioner'sCommissioner's Office</span></a><span> (ICO) on individuals rights under the General Data Protection Regulation.</span></p>
			<p><strong>&nbsp;</strong></p>
			<p><strong>If at any time you would like to unsubscribe from receiving future emails, you can either:</strong></p>
			
			<ul>
			<li><span>Follow the unsubscribe link at the bottom of any automated email we send</span></li>
			<li><span>Please email us directly and request to be removed from some or all email subscriptions</span></li>
			</ul>
			
			<p><span>We may change this privacy policy from time to time. When we do, we will inform you via email.</span></p>


		</div>
	</div>
</div>
<seen-enough title='Interested in <span class="text-primary">growing</span> your business?' sentence="Our strategies help businesses strengthen their relationships with their customers online." :link="'/contact'" btntext="Let’s Talk"></seen-enough>
@endsection