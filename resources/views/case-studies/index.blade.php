@php
$page = 'Case Studies';
$pagetitle = 'Case Studies - The Growth Company';
$metadescription = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud et amiss';
$pagetype = 'dark';
$pagename = 'case-studies';
$ogimage = 'https://thegrowthcompany.ie/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5 mob-pb-0 mt-5">
	<div class="row mt-5 pt-5">
		<div class="col-12 text-center text-lg-left">
			<h1>Case Studies</h1>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large">Check out our case studies below:</p>
		</div>
	</div>
	

</header>
@endsection
@section('content')
<case-studies-index :categories="{{$categories}}"></case-studies-index>
<seen-enough title='Interested in <span class="text-primary">growing</span> your business?' sentence="Our strategies help businesses strengthen their relationships with their customers online." :link="'/contact'" btntext="Let’s Talk"></seen-enough>
@endsection