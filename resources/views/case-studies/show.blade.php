@php
$page = 'Case Studies';
$pagetitle = $post->title . ' | The Growth Company';
$metadescription = $post->meta_description;
$keywords = $post->keywords;
$pagetype = 'dark';
$pagename = 'case-studies';
$ogimage = 'https://thegrowthcompany.ie' . $post->getFirstMediaUrl('normal');
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'keywords' => $keywords, 'ogimage' => $ogimage])
@section('fbroot')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v4.0"></script>
@endsection
@section('header')
<header class="container-fluid position-relative bg bg-down-up z-1 menu-padding mb-5 mob-mb-0">
  <div class="row mt-5 pt-5 mob-mt-0">
    <div class="container">
      <div class="row justify-content-center pt-5">
        <div class="col-lg-6 pt-5 ipadp-pt-0 mob-pt-0 mob-mt-0 text-center text-lg-left mob-mb-3 d-none d-lg-block">
          <p class=""><a href="{{route('case-studies.index')}}" class=" text-primary"><i class="fa fa-angle-double-left mr-2"></i><b>Browse all case studies</b></a></p>
          <h1 class="mb-3 case-study-title">{{$post->title}}</h1>
          <p class="text-large mb-4">{{$post->excerpt}}</p>
          <p class="mb-1"><b>Share this case study:</b></p>
          <p class="text-larger">
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-primary">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-primary">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-primary">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-primary">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
        <div class="col-lg-6 px-5">
          <picture>
            <source media="(min-width: 768px)" srcset="{{$post->getFirstMediaUrl('casestudies','normal-webp')}}" type="image/webp"/> 
            <source media="(min-width: 768px)" srcset="{{$post->getFirstMediaUrl('casestudies','normal')}}" type="{{$post->getFirstMedia('casestudies')->mime_type}}"/> 
            <source media="(min-width: 1px)" srcset="{{$post->getFirstMediaUrl('casestudies','featured-webp')}}" type="image/webp"/> 
            <source media="(min-width: 1px)" srcset="{{$post->getFirstMediaUrl('casestudies','featured')}}" type="{{$post->getFirstMedia('casestudies')->mime_type}}"/> 
            <img src="{{$post->getFirstMediaUrl('casestudies','featured')}} 1w,  {{$post->getFirstMediaUrl('casestudies','normal')}} 768w, {{$post->getFirstMediaUrl('casestudies','double')}} 1200w" type="{{$post->getFirstMedia('casestudies')->mime_type}}" alt="{{$post->title}}" class="w-100 backdrop-content lazy" />
          </picture>
        </div>
        <div class="col-12 pt-5 ipadp-pt-0 mob-mt-0 text-center text-lg-left mob-mb-3 d-lg-none">
          <p class=""><a href="{{route('case-studies.index')}}" class=" text-primary"><i class="fa fa-angle-double-left mr-2"></i><b>Browse all case studies</b></a></p>
          <h1 class="mb-3 case-study-title">{{$post->title}}</h1>
          <p class="text-large mb-4">{{$post->excerpt}}</p>
          <p class="mb-1"><b>Share this case study:</b></p>
          <p class="text-larger">
            <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-primary">
              <i class="fa fa-facebook"></i>
            </a>
            <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-primary">
              <i class="fa fa-linkedin ml-2"></i>
            </a>
            <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-primary">
              <i class="fa fa-twitter ml-2"></i>
            </a>
            <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-primary">
              <i class="fa fa-whatsapp ml-2"></i>
            </a>
          </p>
        </div>
      </div> 
    </div>
  </div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
  <div class="row pb-5 text-center text-lg-left">
    <div class="col-lg-6">
      <picture>
        <source srcset="/img/graphics/bulb.webp" type="image/webp"/> 
        <source srcset="/img/graphics/bulb.jpg" type="image/jpeg"/> 
        <img src="/img/graphics/bulb.jpg" type="image/jpeg" alt="The Growth Company background bulb" width="670" height="510" class="w-100 h-auto case-studies-blub lazy mt-5"/>
      </picture>
    </div>
    <div class="col-lg-6 mob-mt-0 blog-body mob-pt-5">
      <div class="d-table w-100 h-100">
        <div class="d-table-cell align-middle w-100 text-white">  
          <h2>How we helped</h2>
          <div class="line line-primary my-4 text-left"><span class="ml-0 mob-mx-auto"></span></div>
          {!!$post->how!!}
        </div>
      </div>
    </div>
  </div>
  <div class="row mt-5">  
    <div class="col-lg-6 mob-mt-0 blog-body text-center text-lg-left">
      <div class="d-table w-100 h-100">
        <div class="d-table-cell align-middle w-100 text-white">  
          <img src="/img/icons/quote.svg" width="88" height="77" alt="The growth company quote icon" class="mb-4" /> 
          {!!$post->testimonial!!}
          <p class="mt-3"><b>{{$post->testimonial_name}}</b></p>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <picture>
        <source srcset="/img/graphics/hands.webp" type="image/webp"/> 
          <source srcset="/img/graphics/hands.jpg" type="image/jpeg"/> 
          <img src="/img/graphics/hands.jpg" type="image/jpeg" alt="The Growth Company background hands" width="670" height="510" class="w-100 h-auto d-none d-lg-block lazy mt-5"/>
      </picture>
    </div>
    <div class="col-12 mt-5  text-center text-lg-left">
      <p class="mb-1 text-large"><b>Share this case study:</b></p>
      <p class="text-larger">
          <a href="https://facebook.com/sharer/sharer.php?u={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-fb text-primary">
          <i class="fa fa-facebook"></i>
        </a>
        <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{Request::fullUrl()}}&amp;title={{urlencode($post->title)}}&amp;summary={{urlencode($post->title)}}&amp;source={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-ln text-primary">
          <i class="fa fa-linkedin ml-2"></i>
        </a>
        <a href="https://twitter.com/intent/tweet/?text={{urlencode($post->title)}}&amp;url={{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="social-btn social-btn-tw text-primary">
          <i class="fa fa-twitter ml-2"></i>
        </a>
        <a href="whatsapp://send?text={{urlencode($post->title)}}%20{{Request::fullUrl()}}" target="_blank" rel="noopener" aria-label="" class="d-sm-none social-btn social-btn-wa text-primary">
          <i class="fa fa-whatsapp ml-2"></i>
        </a>
      </p>
    </div>
  </div> 
</div>
<seen-enough title='More case studies from The Growth Company' :link="'{{route('case-studies.index')}}'" :btntext="'Case Studies'"></seen-enough>
@endsection
@section('scripts')
<script>
    $(window).load(function(){
        $('.ql-video').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
        $('.fr-video iframe').each(function(i, e){
            var width = $(e).width();
            $(e).css({
                "width": width,
                "height": width*(9/16)
            });
        });
    });
</script>
@endsection