@php
$page = 'Homepage';
$pagetitle = "The Growth Company - Helping businesses strengthen relationships with their customers";
$metadescription = "Our strategies help businesses strengthen their relationships with their customers online.";
$pagetype = 'dark';
$pagename = 'home';
$ogimage = 'https://thegrowthcompany.ie/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<home-header></home-header>
@endsection
@section('content')

<div class="container mt-minus pt-5 mob-px-4 position-relative z-3 ">
    <div class="row">
        <services-carousel></services-carousel>
    </div>
</div>
<div class="container position-relative z-2 my-5 py-5 mob-px-4">
    <div class="row mt-5 py-5 text-center text-lg-left">
        <div class="col-12">
            <p class="mimic-h2 mb-5">Who We Work With<span class="text-primary">:</span></p>
        </div>
        <div class="col-lg-4 mb-5 border-lg-right mob-pl-3">
            <p class="text-larger mb-0 line-height-1-3"><b>Entrepreneurs, Coaches and Consultants</b></p>
            <p class="mb-0">Master LinkedIn Social Selling and turn contacts into contracts.</p>
        </div>
        <div class="col-lg-4 pl-5 border-lg-right mb-5 mob-pl-3">
            <p class="text-larger mb-0 line-height-1-3"><b>Business Development and Sales Teams</b></p>
            <p class="mb-0">Enable your teams to confidently use LinkedIn to prospect and deepen customer relationships.</p>
        </div>
        <div class="col-lg-4 pl-5 mb-5 mob-pl-3">
            <p class="text-larger mb-0 line-height-1-3"><b>Small and Medium Sized Businesses</b></p>
            <p class="mb-0">Boost your staff’s LinkedIn skills, improve performance and increase social media ROI.</p>
        </div>
        <div class="col-lg-8">
            <p class="text-larger"><b>Do you want increased referrals, appointments and sales using LinkedIn? Talk to us today.</b></p>
            <a href="/contact">
                <button type="button" class="btn btn-primary btn-icon">Let's Talk <i aria-hidden="true" class="fa fa-arrow-circle-o-right"></i></button>
            </a>
        </div>
    </div>
    <div class="row pt-5 mob-pt-0">
        <div class="col-12 mt-5 text-center text-lg-left">
            <div class="line line-primary mb-4"><span class="ml-0 mob-mx-auto"></span></div>
            <p class="mimic-h3 mb-4">We are The Growth Company, advisors and experts on growth, strategy and innovation.</p>
            <p class="text-large mb-5">Working with <span class="text-primary">...</span></p>
        </div>
        <client-logos></client-logos>
    </div>
</div>
<div class="container-fluid position-relative z-2 py-5 mob-px-4">
    <picture>
        <source srcset="/img/graphics/globe-2.webp" type="image/webp"/> 
        <source srcset="/img/graphics/globe-2.png" type="image/png"/> 
        <img src="/img/graphics/globe-2.png" type="image/png" alt="The Growth Company background globe 2" width="1170" height="619" class="lazy bg-left"/>
    </picture>
    <div class="row pt-5 mob-pt-0">
        <div class="col-12 text-center mb-5">
            <p class="mimic-h3 mb-5 pb-4">Don’t just take our word for it…</p>
        </div>
        <testimonials></testimonials>
    </div>
</div>
<div class="container mt-5 mob-px-4">
    <div class="row">
        <div class="col-12 mt-5 text-center text-lg-left">
            <p class="mimic-h2">Our Experience</p>
            <div class="line line-primary my-4"><span class="ml-0 mob-mx-auto"></span></div>
            <p class="text-large">The Growth Company is Ireland’s leading LinkedIn Training and Consulting company providing expertise to individuals and corporate teams throughout the island and beyond.</p>

            <p class="text-large">We help companies master LinkedIn, build positive relationships with decision-makers in their target markets, and generate opportunities that align with their areas of expertise and growth plans.</p>

            <p class="text-large">We also help our clients manage their social media, become thought leaders in their industry, and strategically and relationally grow their businesses with B2B content marketing and storytelling.</p>

            <p class="text-large mb-4">If you want to generate more leads, have more relevant conversations with decision-makers, and get increased referrals, appointments, and sales using LinkedIn, then talk to The Growth Company today!</p>
            <a href="/contact"><button class="btn btn-primary btn-icon">Let’s Talk <i aria-hidden="true" class="fa fa-arrow-circle-o-right"></i></button></a>
        </div>
    </div>
</div>
<div class="container py-5 position-relative z-2 my-5 mob-px-4">
    <div class="row pt-5 mob-pt-0">
        <div class="col-12 text-center text-lg-left">
            <p class="mimic-h3">Stay up to date</p>
            <div class="line line-primary my-4"><span class="ml-0 mob-mx-auto"></span></div>
        </div>
        <div class="col-lg-6 text-center text-lg-left">
            <p class="pr-5 mob-px-0">Sign up to our mailing list to stay up to date with our latest articles & offers!</p>
            <ul class="d-lg-none text-left mob-pl-5">
                <li data-aos="fade-up" data-aos-delay="200"><p>Exculsive tips & offers</p></li>
                <li data-aos="fade-up" data-aos-delay="300"><p>Join a vibrant community of like-minded people</p></li>
                <li data-aos="fade-up" data-aos-delay="400"><p>No junk mail or sales pitches</p></li>
            </ul>
            <mailing-list></mailing-list>
        </div>
        <div class="col-lg-6 pl-5 mob-px-3 d-none d-lg-block">
            <ul>
                <li data-aos="fade-up" data-aos-delay="200"><p>Exculsive tips & offers</p></li>
                <li data-aos="fade-up" data-aos-delay="300"><p>Join a vibrant community of like-minded people</p></li>
                <li data-aos="fade-up" data-aos-delay="400"><p>No junk mail or sales pitches</p></li>
            </ul>
        </div>
    </div>
</div>
<div class="container py-5 mob-px-4 mob-px-4">
    <div class="row">
        <div class="col-12 text-center text-lg-left">
            <p class="mimic-h2">Latest Blog Posts</p>
            <div class="line line-primary my-4"><span class="ml-0 mob-mx-auto"></span></div>
        </div>
        <news-inline></news-inline>
        <div class="col-12 mt-4 mob-mt-0 text-center text-lg-left">
            <p class="text-large mb-0"><a href="/blog" class="text-primary"><b>Read more blog posts</b><i class="fa fa-angle-double-right ml-2"></i></a></p>
        </div>
    </div>
</div>
<seen-enough title='Interested in <span class="text-primary">growing</span> your business?' sentence="Our strategies help businesses strengthen their relationships with their customers online." :link="'/contact'" btntext="Let’s Talk"></seen-enough>
@endsection
@section('scripts')

@endsection