@php
$page = 'About';
$pagetitle = 'About - The Growth Company';
$metadescription = "We are The Growth Company, advisors and experts on social selling and LinkedIn for business. We partner with you to scale your business for the digital buyer.";
$pagetype = 'dark';
$pagename = 'about';
$ogimage = 'https://thegrowthcompany.ie/img/og.jpg';
@endphp
@extends('layouts.maintenance', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5 mob-pb-0 mt-5 mob-mt-0">
	<div class="row mt-5 pt-5 justify-content-center">
		<div class="col-lg-9 text-center text-lg-left">
		    <div class="position-relative z-2 text-center">
		    	<img src="/img/logos/logo.svg" alt="The Growth Company Logo" width="220" class="mb-5" />
				<p class="mimic-h3 mb-5">Website undergoing maintenance, please check back later.</p>
				<p><a href="mailto:hello@thegrowthcompany.ie"><i class="fa fa-envelope-o mr-1"></i> hello@thegrowthcompany.ie</a></p>
	            <p><a href="tel:00447512685946"><i class="fa fa-phone mr-1"></i> +44 (0) 7512 685 946</a></p>
	            <p class="text-center mt-3">
	              <a href="https://www.linkedin.com/company/thegrowthcompanyie/" target="_blank" aria-label="LinkedIn" rel="noreferrer"><i class="fa fa-linkedin mob-mx-0"></i></a>
	              <a href="https://www.facebook.com/thegrowthcompanyie" aria-label="Facebook" rel="noreferrer"><i class="fa fa-facebook ml-3"></i></a>
	              <a href="https://www.instagram.com/thegrowthcompanyie/" aria-label="Instagram" rel="noreferrer"><i class="fa fa-instagram ml-3"></i></a>
	              <a href="https://twitter.com/growthcompanyie" aria-label="Twitter" rel="noreferrer"><i class="fa fa-twitter ml-3"></i></a>
	            </p>
			</div>
		</div>
	</div>
</header>
@endsection
@section('content')

@endsection	