@php
$page = 'Contact';
$pagetitle = 'Contact - The Growth Company';
$metadescription = "Contact us";
$pagetype = 'dark';
$pagename = 'contact';
$ogimage = 'https://thegrowthcompany.ie/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container pt-5 mb-3 mob-pb-0 mt-5">
	<div class="row mt-5 pt-5 mob-pt-0">
		<div class="col-12 text-center text-lg-left">
			<h1>Lets Talk</h1>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-8">
        	<contact-page-form :recaptcha="'{{env('GOOGLE_RECAPTCHA_KEY')}}'" :page="'{{$page}}'"></contact-page-form>
    	</div>
    	<div class="col-lg-4 mb-5 pl-5 mob-px-3 mob-mt-5 text-center text-lg-left">
          <p class="mimic-h3 mb-3">Contact Us</p>
          <div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
          <p class="mb-3 text-one"><a href="mailto:hello@thegrowthcompany.ie"><b><span class="text-white"><i class="fa fa-envelope-o mr-2"></i></span> hello@thegrowthcompany.ie</b></a></p>
          
          <p class="mb-4 text-one"><a href="tel:00447512685946"><b><span class="text-white"><i class="fa fa-phone mr-2"></i></span> +44 (0) 7512 685 946</b></a></p>
          <p class="mb-0 text-large mt-3">
            <a href="https://www.linkedin.com/company/thegrowthcompanyie/" target="_blank" aria-label="LinkedIn" rel="noreferrer"><i class="fa fa-linkedin"></i></a>
              <a href="https://www.facebook.com/thegrowthcompanyie" aria-label="Facebook" rel="noreferrer"><i class="fa fa-facebook ml-3"></i></a>
              <a href="https://www.instagram.com/thegrowthcompanyie/" aria-label="Instagram" rel="noreferrer"><i class="fa fa-instagram ml-3"></i></a>
              <a href="https://twitter.com/growthcompanyie" aria-label="Twitter" rel="noreferrer"><i class="fa fa-twitter ml-3"></i></a>
          </p>
        </div>
	</div>
</div>
<seen-enough title='Interested in <span class="text-primary">growing</span> your business?' sentence="Our strategies help businesses strengthen their relationships with their customers online." :link="'/services'" btntext="Our Services"></seen-enough>
@endsection