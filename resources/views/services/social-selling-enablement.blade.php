@php
$page = 'Services';
$pagetitle = 'Social Selling Enablement - Services - The Growth Company';
$metadescription = "A six-month structured programme to scale Social Selling from within your organisation";
$pagetype = 'dark';
$pagename = 'strategy-and-consulting';
$ogimage = 'https://thegrowthcompany.ie/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5 mob-pb-0 mt-5">
	<div class="row mob-pt-5">
		<div class="col-lg-6 text-center text-lg-left">
			<div class="d-table w-100 h-100">
				<div class="d-table-cell align-middle w-100 position-relative z-2">
					<h1>Social Selling Enablement</h1>
					<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
					<p class="text-large">A six-month structured programme to scale Social Selling from within your organisation.</p>
					<a href="/contact">
						<button type="button" class="btn btn-primary">Lets Talk</button>
					</a>
				</div>
			</div>
			<picture>
		    	<source srcset="/img/graphics/bulb.webp" type="image/webp"/> 
		        <source srcset="/img/graphics/bulb.jpg" type="image/jpeg"/> 
		        <img src="/img/graphics/bulb.jpg" type="image/jpeg" alt="The Growth Company background bulb" width="670" height="510" class="h-auto lazy mt-5 mob-bulb-2 d-lg-none"/>
		    </picture>
		</div>
		<div class="col-lg-6 d-none d-lg-block text-right">
			<picture>
		    	<source srcset="/img/graphics/bulb.webp" type="image/webp"/> 
		        <source srcset="/img/graphics/bulb.jpg" type="image/jpeg"/> 
		        <img src="/img/graphics/bulb.jpg" type="image/jpeg" alt="The Growth Company background bulb" width="670" height="510" class="w-100 h-auto bulb-2 lazy mt-5"/>
		    </picture>
  		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mt-5 mob-px-4">
	<div class="row text-center text-lg-left">
		<div class="col-lg-4 mb-5">
			<p class="text-larger line-height-1-3"><b>Maximise Sales Navigator ROI</b></p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="mb-5">Give your team the transformational skills to prospect effectively with LinkedIn Sales Navigator—and see how powerful it is and why it drives 35% larger deals with decision-makers and 45% more opportunities.</p>
		</div>
		<div class="col-lg-4 mb-5">
			<p class="text-larger line-height-1-3"><b>Embed Social Selling Habits</b></p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="mb-5">Regular webinars and 1:1 mentoring will embed Social Selling habits over time. These proven approaches will build the architecture to sustain long-term growth goals, year over year, while continuously enhancing your buyer’s experience.</p>
		</div>
		<div class="col-lg-4 mb-5">
			<p class="text-larger line-height-1-3"><b>Activate Social Selling Champions</b></p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="mb-5">You will identify Social Selling Champions who will act as internal peer mentors and nurture others within your organisation and designed to scale up your sales and marketing machine for the digital buyer.</p>
		</div>
		<div class="col-12">
			<p class="mimic-h3 mb-5">Learn How To Successfully Use LinkedIn To Attract New Business In This <span class="text-primary">Free Video</span></p>
			<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/482127303?h=1fe98c1dd2&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div>
		</div>
	</div>
	<div class="row my-5 py-5">
		<div class="col-12">
			<p class="mimic-h2 text-center text-lg-left">Social Selling Enablement</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large text-center text-lg-left"><b>More than half of the world’s population now uses social media</b>, with more people using social media than not. In 2020, social media users grew by more than 10%. With the ongoing Coronavirus continuing to influence and reshape various aspects of our lives, the global digital landscape is continuing to evolve rapidly. </p>

			<p class="text-large text-center text-lg-left">With so many people now online, <b>it has changed buyers’ habits, and it’s increasingly difficult to reach them early enough in their decision-making process</b>. Suddenly developing relationships with buyers through social networks has become an increasingly critical skill.</p>

			<p class="text-large text-center text-lg-left">Social selling allows your sales team to react to this change in buyer behaviour. It is a way of getting the salesperson back into the buying process to influence the decision-making process, <b>increase win rates, decrease sales cycles and turn contacts into contracts</b>.</p>

			<p class="text-large text-center text-lg-left">Martin and his team will help you define objectives and goals specific to your organisation so that you can review progress and see a real ROI.</p>

			<p class="mimic-h3 mt-5 text-center text-lg-left">Why Choose Social Selling Enablement?</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large text-center text-lg-left">Your organisation is complacent and will continue selling as it always has done before. You have not established with your sales team the mindset that social and digital communications will positively impact the business.</p>

			<p class="text-large text-center text-lg-left">You or someone in your sales team likely has confused LinkedIn and social selling as the same. As a result, you’ve probably confronted the problem with the same playbook you’ve always run:</p>

			<ul>
				<li><p class="text-large text-center text-lg-left">Conduct a half-day, in-person workshop or register to attend an online LinkedIn masterclass.</p></li>
				<li><p class="text-large text-center text-lg-left">Signed up for multiple LinkedIn Sales Navigator licences.</p></li>
			</ul>
			<p class="text-large text-center text-lg-left">If this is the path you choose, I can tell you with almost 100% certainty that your organisation will never digitally transform into a social selling machine, especially a machine that moves the sales needle. No half-day workshops/online LinkedIn masterclasses or Sales Navigator licences will drive millions of pounds into your sales pipeline.</p>

			<p class="text-large text-center text-lg-left">The root cause for these half-baked sales enablement solutions is a lack of commitment to real behaviour change. If this sounds like your business, you will benefit from Social Selling Enablement, a six-month programme that will implement social selling across your organisation, and give you a significant sales uplift .</p>
			<p class="text-large text-center text-lg-left">The programme is fundamentally not about LinkedIn, but it is really about new ways of thinking—as leaders give the organisation the roadmap to scaling social selling strategy including digital maturity and investment models, risk, and technologies. And how to leverage the latest, consistently successful methods to reach and engage customers online.</p>

		</div>
		<div class="col-lg-6 mt-5 pt-5">
			<p class="mimic-h3 mb-4 text-center text-lg-left">What’s<br/>included:</p>
			<ul>
				<li>Creating a mindset shift for Digital Transformation</li>
				<li>Dedicated Support From Martin and team</li>
				<li>Regular Executive Coaching Sessions On Zoom</li>
				<li>Monthly Social Selling Webinars</li>
				<li>Individual Success Mentoring</li>
				<li>2 Half Day Workshops For Up To 12 People/Social Champions</li>
				<li>Digital and Social Media Strategies For Success</li>
			</ul>
		</div>
		<div class="col-lg-6 mt-5 pt-5">
			<p class="mimic-h3 mb-4 text-center text-lg-left">Expected<br/>Outcomes:</p>
			<ul>
				<li>Activate a powerful Digital Footprint </li>
				<li>Gain more influence in early-stage buying decisions</li>
				<li>Add value and increase knowledge in the buying process</li>
				<li>Attract qualified sales leads and inbound enquiries</li>
				<li>Master the latest, consistently successful methods including Sales Navigator to reach and engage customers online</li>
				<li>Generate increased referrals, appointments and sales</li>
				<li>Increase LinkedIn Social Selling Index (SSI) Score</li>
			</ul>
		</div>
		<div class="col-lg-6 mt-5 pt-5">
			<p class="mimic-h3 mb-4 text-center text-lg-left">Ideal For Businesses That Want To: </p>
			<ul class="pr-5 mob-px-4">
				<li>Use LinkedIn purposefully to build social trust and create a high-quality community</li>
				<li>Build long-term trust real buyers are looking for in the marketplace</li>
				<li>Develop real influence and authority in your sector and connect with decision-makers </li>
				<li>Strategically target and increase value and add knowledge throughout the buying process</li>
				<li>Scale social selling across the business, including Digital Maturity and investment models, managing risk and privacy online</li>
				<li>Want to see a better ROI for existing Sales Navigator Licences</li>
			</ul>
		</div>
		<div class="col-lg-6 mt-5 pt-5">
			<p class="mimic-h3 mb-4 text-center text-lg-left">Social Selling Enterprise Webinar Series</p>
			<p class="text-small text-primary text-center text-lg-left"><i>Click numbers 1-4 to read more</i></p>
			<div class="accordion" id="workshopAccordion">
				<div class="">
					<div id="headingOne">
						<p class=""><b><a class="text-left" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><span class="text-primary">1. </span> 3 Hour Meeting To Get Started <i class="d-none d-lg-inline fa fa-chevron-down ml-3 text-primary"></i></a></b></p>
					</div>
					<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#workshopAccordion">
						<ul class="pl-5">
							<li>Review organisations sales metrics and processes to date.</li>
							<li>Set Social Selling objectives </li>
							<li>Outline roles and responsibilities </li>
							<li>Understand metrics for Social Selling success</li>
							<li>Setting up Google Alerts/Social Listening tools</li>
							<li>Running a competitor analysis</li>
							<li>Plan customisation of workshop and training material</li>
						</ul>
					</div>
				</div>
				<div class="">
					<div class="" id="headingTwo">
						<p class=""><b><a class="text-left" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><span class="text-primary">2.</span> Phase 1 Full Day Workshop <i class="d-none d-lg-inline fa fa-chevron-down ml-3 text-primary"></i></a></b></p>
					</div>
					<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#workshopAccordion">
						<div class="">
							<ul class="pl-5">
								<li>Introduction to Social Selling</li>
								<li>Understanding Visibility and Credibility</li>
								<li>Build A All-Star Buyer-Centric LinkedIn Profile</li>
								<li>Understand Privacy and Settings</li>
								<li>How To Find The Right Keywords To Use</li>
								<li>Write A Bio/Summary Section That Converts</li>
								<li>Making The Shift From Resume To Reputation</li>
								<li>Positioning and Messaging For Competitive Advantage</li>
								<li>Understanding Your Social Selling Index Score (SSI)</li>
								<li>Applying Boolean Search To Find Your Target Market</li>
								<li>Leverage Company Searches To Find Prospects</li>
								<li>Research Prospects and Companies</li>
								<li>Using Data Analytics For Competitive Advantage</li>
								<li>Know Difference Between Lead & Account Searching*</li>
								<li>Saving Leads & Accounts, Lists and Searches*</li>
								<li>Leveraging Sales Spotlight To Segment Data For Better Results*</li>
								<li>Segment Prospects Using Tags & Notes* </li>
							</ul>
							<p class="pl-4 text-small">* LinkedIn Sales Navigator Features</p>
						</div>
					</div>
				</div>
				<div class="">
					<div class="" id="headingThree">
						<p class=""><b><a class="text-left" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><span class="text-primary">3.</span> Phase 2 Full Day Workshop <i class="d-none d-lg-inline fa fa-chevron-down ml-3 text-primary"></i></a></b></p>
					</div>
					<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#workshopAccordion">
						<div class="">
							<ul class="pl-5">
								<li>Talking To Strangers On LinkedIn</li>
								<li>How To Prepare Before Reaching Out </li>
								<li>Understanding Degree’s of Connections </li>
								<li>Personalising Connection Requests</li>
								<li>Crafting Warm Introductions</li>
								<li>Leveraging Technology For Smart Insights</li>
								<li>Connection Requests Do’s and Don’ts</li>
								<li>Why Not To Connect with Everybody and Anybody </li>
								<li>How to Correctly Use In-Mail*</li>
								<li>Back Up Your Connections</li>
								<li>The Content Mix That Gives You Visibility</li>
								<li>The Importance Of Familiarity </li>
								<li>Posting Relevant and Original Content</li>
								<li>Sharing Success And Why To Do It</li>
								<li>Understanding Hashtags and @Mentions</li>
								<li>Best Times To Reach Target Buyers</li>
								<li>Tools That Automate and Schedule</li>
								<li>Measuring Influence and Amplification </li>
								<li>What’s Not To Post On LinkedIn</li>
								<li>Why Spelling and Grammar Matter</li>
								<li>Leveraging Social Listening Tools As Triggers</li>
								<li>How To Spy On Competitors Without Them Knowing</li>
							</ul>
							<p class="pl-4 text-small">* LinkedIn Sales Navigator Features</p>
						</div>
					</div>
				</div>
				<div class="">
					<div class="" id="headingFour">
						<p class=""><b><a class="text-left" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour"><span class="text-primary">4.</span> Monthly Social Selling Support <i class="d-none d-lg-inline fa fa-chevron-down ml-3 text-primary"></i></a></b></p>
					</div>
					<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#workshopAccordion">
						<div class="">
							<ul class="pl-5">
								<li>Post-training 1:1 mentoring and coaching sessions with Martin</li>
								<li>Monthly Social Selling webinar masterclasses</li>
								<li>LinkedIn Sales Navigator Support</li>
								<li>Access to Martin a dedicated Slack channel.</li>
								<li>Weekly reviews and measurement of analytics</li>
								<li>Sostac Digital Marketing Strategy </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12 mb-4 text-center text-lg-left">
			<p class="mimic-h2 mb-4">Pricing and Delivery Options</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
		</div>
		<div class="col-lg-6 mb-4">
			<div class="card bg-primary px-4 py-5">
				<p class="mimic-h3 mb-0 text-center">Social Selling<br/>Enablement</p>
				<p class="text-center">Six Month Programme</p>
				<p class="text-large text-center mb-0"><b>£3000 Per Month For Six Months</b></p>
				<div class="line line-white my-4 text-center text-lg-left"><span class="mx-auto"></span></div>
				<ul>
					<li>Dedicated Support From Martin and team </li>
					<li>3 Hour Kick Off Meeting To Identify Social Selling Objective and Metrics for Success</li>
					<li>2 Full Day Social Selling Workshops for up to 12 People / Social Selling Champions</li>
					<li>2 x 1 hour Zoom Coaching Sessions per workshop attendee</li>
					<li>6 x 30 minute Digital Marketing and Social Selling webinar masterclasses (one per month)</li>
					<li>Monthly LinkedIn Sales Navigator analytics support including SSI Index Score Review and Improvement</li>
					<li>Recordings Available</li>
					<li>Individual and Team Accountability after each session</li>
				</ul>
				<p class="text-larger text-center mt-3 mb-2"><b>Interested?</b></p>
				<a href="/contact" class=" text-center">
					<button type="button" class="btn btn-white mx-auto">Let's Talk</button>
				</a>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid position-relative z-2 py-5">
    <picture>
        <source srcset="/img/graphics/globe-2.webp" type="image/webp"/> 
        <source srcset="/img/graphics/globe-2.png" type="image/png"/> 
        <img src="/img/graphics/globe-2.png" type="image/png" alt="The Growth Company background globe 2" width="1170" height="619" class="lazy bg-left"/>
    </picture>
    <div class="row pt-5 mob-pt-0">
        <div class="col-12 text-center mb-5">
            <p class="mimic-h3 mb-5 pb-4">Hear from our clients…</p>
        </div>
        <testimonials></testimonials>
    </div>
</div>
<seen-enough title='Interested in Strategy & Consulting?' sentence="LinkedIn marketing and social selling consulting to turn contacts into contracts." :link="'/contact'" btntext="Let’s Talk"></seen-enough>
@endsection
@section('scripts')
<script src="https://player.vimeo.com/api/player.js"></script>
@endsection