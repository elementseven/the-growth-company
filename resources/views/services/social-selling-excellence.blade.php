@php
$page = 'Services';
$pagetitle = 'Social Selling Excellence - Services - The Growth Company';
$metadescription = "One-To-One Coaching for B2B companies, coaches and consultants who want increased referrals, appointments and sales using LinkedIn.";
$pagetype = 'dark';
$pagename = 'strategy-and-consulting';
$ogimage = 'https://thegrowthcompany.ie/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5 mob-pb-0 mt-5">
	<div class="row mob-pt-5">
		<div class="col-lg-6 text-center text-lg-left">
			<div class="d-table w-100 h-100">
				<div class="d-table-cell align-middle w-100 position-relative z-2">
					<h1>Social Selling Excellence</h1>
					<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
					<p class="text-large">One-To-One Coaching for B2B companies, coaches and consultants who want increased referrals, appointments and sales using LinkedIn.</p>
					<a href="/contact">
						<button type="button" class="btn btn-primary">Lets Talk</button>
					</a>
				</div>
			</div>
			<picture>
				<source srcset="/img/graphics/socialselling.webp" type="image/webp"/> 
				<source srcset="/img/graphics/socialselling.png" type="image/png"/> 
				<img src="/img/graphics/socialselling.png" type="image/png" alt="The Growth Company background Social Selling" width="670" height="510" class="h-auto lazy mt-5 mob-socialselling-2 d-lg-none"/>
			</picture>
		</div>
		<div class="col-lg-6 d-none d-lg-block">
			<picture>
				<source srcset="/img/graphics/socialselling.webp" type="image/webp"/> 
				<source srcset="/img/graphics/socialselling.png" type="image/png"/> 
				<img src="/img/graphics/socialselling.png" type="image/png" alt="The Growth Company background Social Selling" width="670" height="510" class="h-auto lazy mt-5" style="margin-top: -100px;"/>
			</picture>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mt-5 mob-px-4">
	<div class="row text-center text-lg-left">
		<div class="col-lg-4 mb-5">
			<p class="text-larger line-height-1-3"><b>Embed Change Behaviour And Scale Social Selling</b></p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="mb-5">Social Selling is fundamentally about better ways of doing business—and 1-2-1 support is a great way to embed behaviour change and increase your sales and marketing cadence.</p>
		</div>
		<div class="col-lg-4 mb-5">
			<p class="text-larger line-height-1-3"><b>Tailored Support To Suit Specific Business Roles </b></p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="mb-5">Coaching can support specific individuals, making sure you get the best out of LinkedIn and Sales Navigator. Tailored to the individual’s role be that Company Director, Sales Manager, Marketing Consultant, Business Coach or Inside Sales.</p>
		</div>
		<div class="col-lg-4 mb-5">
			<p class="text-larger line-height-1-3"><b>Demonstrate Better Sales Navigator ROI</b></p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="mb-5">Master LinkedIn Sales Navigator—making it easy to establish and grow relationships with prospects and customers by tapping into the full power of LinkedIn, the world’s largest professional network of 750M + members.</p>
		</div>
		<div class="col-12">
			<p class="mimic-h3 mb-5">Learn How To Successfully Use LinkedIn To Attract New Business <span class="text-primary">Free Video</span></p>
			<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/482127303?h=1fa69c04da&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div>
		</div>
	</div>
	<div class="row my-5 py-5">
		<div class="col-12">
			<p class="mimic-h2 text-center text-lg-left">Social Selling Coaching</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large text-center text-lg-left">More than half of the world’s population now uses social media, with more people using social media than not. In 2020, social media users grew by more than 10%. With the ongoing Coronavirus continuing to influence and reshape various aspects of our lives, the global digital landscape is continuing to evolve rapidly.</p>
			<p class="text-large text-center text-lg-left">With so many people online, it has changed buyers’ habits, and it’s increasingly difficult to reach them early enough in their decision-making process.</p>
			<p class="text-large text-center text-lg-left">Suddenly developing relationships with buyers through social networks has become an increasingly critical skill.</p>
			<p class="text-large text-center text-lg-left">Social selling allows you to react to this change in buyer behaviour. It is a way of getting you back into the buying process to influence the decision-making process, increase win rates, decrease sales cycles and turn contacts into contracts.</p>

			<p class="mimic-h3 mt-5 text-center text-lg-left">Why Choose Social Selling Coaching?</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large text-center text-lg-left">Most businesses are complacent and will continue selling as it always has done before. These companies have not accepted that social and digital communications will positively impact their business.</p>
			<p class="text-large text-center text-lg-left">You or someone in your team likely has confused LinkedIn and social selling as the same. As a result, you’ve probably confronted the problem with the same playbook you’ve always run: </p>
			<ul>
				<li><p class="text-large text-center text-lg-left">Conduct a half-day, in-person workshop or register to attend an online LinkedIn masterclass.</p></li>
				<li><p class="text-large text-center text-lg-left">Signed up for multiple LinkedIn Sales Navigator licences.</p></li>
			</ul>

			<p class="text-large text-center text-lg-left">If this is the path you choose, I can tell you with almost 100% certainty that your organisation will never digitally transform into a social selling machine, especially a machine that moves the sales needle. No half-day workshops/online LinkedIn masterclasses or Sales Navigator licences will drive millions of pounds into your sales pipeline. </p>

			<p class="text-large text-center text-lg-left">The root cause for these half-baked sales enablement solutions is a lack of commitment to real behaviour change. If this sounds like your business, you will benefit from Social Selling Coaching to give you the skills to implement social selling across your organisation and give you a significant sales uplift and competitive edge.</p>

		</div>
		<div class="col-lg-6 mt-5 pt-5">
			<p class="mimic-h3 mb-4 text-center text-lg-left">Learning<br/>Objectives:</p>
			<ul>
				<li>Creating a mindset shift for Digital Transformation</li>
				<li>Understand the importance of owning your Community</li>
				<li>Develop a powerful buyer-centric LinkedIn profile</li>
				<li>How to grow your LinkedIn network the right way</li>
				<li>Identify opportunities in LinkedIn groups</li>
				<li>Leverage content to shape the buyer journey</li>
				<li>Use technology to your advantage&nbsp;</li>
				<li>Write In-Mails and send connection requests that convert</li>
			</ul>
		</div>
		<div class="col-lg-6 mt-5 pt-5">
			<p class="mimic-h3 mb-4 text-center text-lg-left">Expected<br/>Outcomes:</p>
			<ul>
				<li>Activate a powerful Digital Footprint </li>
				<li>Gain more influence in early-stage buying decisions</li>
				<li>Add value and increase knowledge in the buying process</li>
				<li>Attract qualified sales leads and inbound enquiries</li>
				<li>Master the latest, consistently successful methods including Sales Navigator to reach and engage customers online</li>
				<li>Generate increased referrals, appointments and sales</li>
				<li>Increase LinkedIn Social Selling Index (SSI) Score</li>
			</ul>
		</div>
		<div class="col-lg-6 mt-5 pt-5">
			<p class="mimic-h3 mb-4 text-center text-lg-left">Ideal For Busy Professionals, Business Owners, Coaches and Consultants That: </p>
			<ul class="pr-5 mob-px-4">
				<li>Use LinkedIn purposefully to build social trust and create a high-quality community</li>
				<li>Build long-term trust real buyers are looking for in the marketplace</li>
				<li>Develop real influence and authority in your sector and connect with decision-makers </li>
				<li>Strategically target and increase value and add knowledge throughout the buying process</li>
				<li>Scale social selling across the business, including Digital Maturity and investment models, managing risk and privacy online</li>
				<li>See a better ROI for existing Sales Navigator Licences</li>
			</ul>
		</div>
		<div class="col-lg-6 mt-5 pt-5">
			<p class="mimic-h3 mb-4 text-center text-lg-left">Social Selling Workshop</p>
			<p class="text-small text-primary text-center text-lg-left"><i>Click numbers 1-6 to read more</i></p>
			<div class="accordion" id="workshopAccordion">
				<div class="">
					<div id="headingOne">
						<p class=""><b><a class="text-left" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><span class="text-primary">1. </span> Setting Yourself Up For Success On LinkedIn <i class="d-none d-lg-inline fa fa-chevron-down ml-3 text-primary"></i></a></b></p>
					</div>
					<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#workshopAccordion">
						<ul class="pl-5">
							<li>Introduction to Social Selling</li>
							<li>Understanding VCCO</li>
							<li>Know Your Target Audience</li>
							<li>Build A All-Star Buyer-Centric LinkedIn Profile</li>
							<li>Understand Privacy and Settings</li>
							<li>How To Find The Right Keywords To Use</li>
							<li>Write A Bio/Summary Section That Converts</li>
							<li>Making The Shift From Resume To Reputation</li>
							<li>Positioning and Messaging For Competitive Advantage</li>
							<li>Add Featured Media and Links To Educate Buyers</li>
							<li>Get High Profile Recommendations </li>
							<li>Strengthen Skills & Endorsements</li>
						</ul>
					</div>
				</div>
				<div class="">
					<div class="" id="headingTwo">
						<p class=""><b><a class="text-left" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><span class="text-primary">2.</span> Prospecting To Build Your Sales Pipeline <i class="d-none d-lg-inline fa fa-chevron-down ml-3 text-primary"></i></a></b></p>
					</div>
					<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#workshopAccordion">
						<div class="">
							<ul class="pl-5">
								<li>Understanding Your Social Selling Index Score (SSI)</li>
								<li>Applying Boolean Search To Find Your Target Market</li>
								<li>Leverage Company Searches To Find Prospects</li>
								<li>Research Prospects and Companies</li>
								<li>Using Data Analytics For Competitive Advantage</li>
								<li>Know Difference Between Lead & Account Searching*</li>
								<li>Saving Leads & Accounts, Lists and Searches*</li>
								<li>Leveraging Sales Spotlight To Segment Data For Better Results*</li>
								<li>Segment Prospects Using Tags & Notes* </li>
							</ul>
							<p class="pl-4 text-small">* LinkedIn Sales Navigator only</p>
						</div>
					</div>
				</div>
				<div class="">
					<div class="" id="headingThree">
						<p class=""><b><a class="text-left" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><span class="text-primary">3.</span> Growing Your LinkedIn Network <i class="d-none d-lg-inline fa fa-chevron-down ml-3 text-primary"></i></a></b></p>
					</div>
					<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#workshopAccordion">
						<div class="">
							<ul class="pl-5">
								<li>Talking To Strangers On LinkedIn</li>
								<li>How To Prepare Before Reaching Out </li>
								<li>Understanding Degree’s of Connections </li>
								<li>Personalising Connection Requests</li>
								<li>Crafting Warm Introductions</li>
								<li>Leveraging Technology For Smart Insights</li>
								<li>Connection Requests Do’s and Don’ts</li>
								<li>Why Not To Connect with Everybody and Anybody </li>
								<li>How to Correctly Use In-Mail*</li>
								<li>Back Up Your Connections</li>
							</ul>
							<p class="pl-4 text-small">* LinkedIn Sales Navigator only</p>
						</div>
					</div>
				</div>
				<div class="">
					<div class="" id="headingFour">
						<p class=""><b><a class="text-left" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour"><span class="text-primary">4.</span> Leveraging Content To Have More Conversations <i class="d-none d-lg-inline fa fa-chevron-down ml-3 text-primary"></i></a></b></p>
					</div>
					<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#workshopAccordion">
						<div class="">
							<ul class="pl-5">
								<li>The Content Mix That Gives You Visibility</li>
								<li>The Importance Of Familiarity </li>
								<li>Posting Relevant and Original Content</li>
								<li>Sharing Success And Why To Do It</li>
								<li>Understanding Hashtags and @Mentions</li>
								<li>Best Times To Reach Target Buyers</li>
								<li>Tools That Automate and Schedule</li>
								<li>Measuring Influence and Amplification </li>
								<li>What’s Not To Post On LinkedIn</li>
								<li>Why Spelling and Grammar Matter</li>
								<li>Leveraging Social Listening Tools As Triggers</li>
								<li>How To Spy On Competitors Without Them Knowing</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="">
					<div class="" id="headingFive">
						<p class=""><b><a class="text-left" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"><span class="text-primary">5.</span> Course Review & LinkedIn Social Selling 5 A Day <i class="d-none d-lg-inline fa fa-chevron-down ml-3 text-primary"></i></a></b></p>
					</div>
					<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#workshopAccordion">
						<div class="">
							<ul class="pl-5">
								<li>Review Topics and Content Covered In Webinar</li>
								<li>Measuring ROI and Criteria For Success</li>
								<li>LinkedIn 5 A Day Daily Routine</li>
								<li>LinkedIn 5 A Day Weekly Team Challenges</li>
								<li>Questions and Answers with Martin</li>
								<li>LinkedIn Sales Navigator Demo*</li>
							</ul>
							<p class="pl-4 text-small">* LinkedIn Sales Navigator only</p>
						</div>
					</div>
				</div>
				<div class="">
					<div class="" id="headingFive">
						<p class=""><b><a class="text-left" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix"><span class="text-primary">6.</span> You Will Receive: <i class="d-none d-lg-inline fa fa-chevron-down ml-3 text-primary"></i></a></b></p>
					</div>
					<div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#workshopAccordion">
						<div class="">
							<ul class="pl-5">
								<li>Weekly Live one-to-one coaching and training sessions to develop and implement social selling behaviour</li>
								<li>LinkedIn Content creation strategy sessions </li>
								<li>Ongoing lead generation guidance </li>
								<li>12 x 60-minute One-To-One Coaching sessions with Martin</li>
								<li>Training includes LinkedIn Sales Navigator  </li>
								<li>Access to the Social Selling Excellence WhatsApp Group and Community</li>
							</ul>
							<p class="pl-4 text-small">* LinkedIn Sales Navigator only</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12 mb-4 text-center text-lg-left">
			<p class="mimic-h2 mb-4">Pricing and Delivery Options</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
		</div>
		<div class="col-lg-6 mb-4">
			<div class="card bg-primary px-4 py-5">
				<p class="mimic-h3 mb-0 text-center">8 Sessions</p>
				<p class="text-center">1 hour per session</p>
				<p class="text-large text-center mb-0"><b>£1170 per individual*</b></p>
				<div class="line line-white my-4 text-center text-lg-left"><span class="mx-auto"></span></div>
				<ul>
					<li>8 x 1 hour Social Selling Coaching Sessions</li>
					<li>Delivered via Zoom (screen sharing)</li>
					<li>Suitable for Company Directors, Inside Sales Managers, Marketing Consultants, Coaches or Senior Decision Makers regardless of organisation or business type.</li>
					<li>LinkedIn SSI Index Score Review and Improvement</li>
					<li>Ongoing Lead Generation Guidance</li>
					<li>Unlimited WhatsApp Support</li>
				</ul>
				<p class="text-small text-center">* Used within 6 months of purchase</p>
				
				<p class="text-larger text-center mt-3 mb-2"><b>Interested?</b></p>
				<a href="/contact" class=" text-center">
					<button type="button" class="btn btn-white mx-auto">Let's Talk</button>
				</a>

			</div>
		</div>
		<div class="col-lg-6 mb-4">
			<div class="card bg-primary px-4 py-5">
				<p class="mimic-h3 mb-0 text-center">16 Sessions</p>
				<p class="text-center">1 hour per session</p>
				<p class="text-large text-center mb-0"><b>£2240 per individual*</b></p>
				<div class="line line-white my-4 text-center text-lg-left"><span class="mx-auto"></span></div>
				<ul>
					<li>16 x 1 hour Social Selling Coaching Sessions</li>
					<li>Delivered via Zoom (screen sharing)</li>
					<li>Suitable for Company Directors, Inside Sales Managers, Marketing Consultants, Coaches or Senior Decision Makers regardless of organisation or business type.</li>
					<li>LinkedIn SSI Index Score Review and Improvement</li>
					<li>Ongoing Lead Generation Guidance</li>
					<li>Unlimited WhatsApp Support</li>
				</ul>				
				<p class="text-small text-center">* Used within 6 months of purchase</p>
				<p class="text-larger text-center mt-3 mb-2"><b>Interested?</b></p>
				<a href="/contact" class=" text-center">
					<button type="button" class="btn btn-white mx-auto mb-3 mob-mb-0">Let's Talk</button>
				</a>

			</div>
		</div>
		<div class="col-lg-6 mb-4">
			<div class="card bg-primary px-4 py-5">
				<p class="mimic-h3 mb-0 text-center">24 sessions</p>
				<p class="text-center">1 hour per session</p>
				<p class="text-large text-center mb-0"><b>£3410 per individual</b></p>
				<div class="line line-white my-4 text-center text-lg-left"><span class="mx-auto"></span></div>
				<ul>
					<li>24 x 1 hour Social Selling Coaching Sessions</li>
					<li>Delivered via Zoom (screen sharing)</li>
					<li>Suitable for Company Directors, Inside Sales Managers, Marketing Consultants, Coaches or Senior Decision Makers regardless of organisation or business type.</li>
					<li>LinkedIn SSI Index Score Review and Improvement</li>
					<li>Ongoing Lead Generation Guidance</li>
					<li>Unlimited WhatsApp Support</li>
				</ul>
				<p class="text-small text-center">* Used within 6 months of purchase</p>
				
				<p class="text-larger text-center mt-3 mb-2"><b>Interested?</b></p>
				<a href="/contact" class=" text-center">
					<button type="button" class="btn btn-white mx-auto">Let's Talk</button>
				</a>

			</div>
		</div>
	</div>
</div>
<div class="container-fluid position-relative z-2 py-5">
    <picture>
        <source srcset="/img/graphics/globe-2.webp" type="image/webp"/> 
        <source srcset="/img/graphics/globe-2.png" type="image/png"/> 
        <img src="/img/graphics/globe-2.png" type="image/png" alt="The Growth Company background globe 2" width="1170" height="619" class="lazy bg-left"/>
    </picture>
    <div class="row pt-5 mob-pt-0">
        <div class="col-12 text-center mb-5">
            <p class="mimic-h3 mb-5 pb-4">Hear from our clients…</p>
        </div>
        <testimonials></testimonials>
    </div>
</div>
<seen-enough title='Interested in Social Selling Excellence?' sentence="LinkedIn marketing and social selling consulting to turn contacts into contracts." :link="'/contact'" btntext="Let’s Talk"></seen-enough>
@endsection