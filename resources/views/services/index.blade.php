@php
$page = 'Services';
$pagetitle = 'Services - The Growth Company';
$metadescription = "Our services";
$pagetype = 'dark';
$pagename = 'services';
$ogimage = 'https://thegrowthcompany.ie/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5 mob-pb-0 mt-5 position-relative z-3">
	<div class="row mt-5 pt-5 mob-mt-0">
		<div class="col-12 text-center text-lg-left">
			<h1>Services</h1>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mob-px-4">
	<div class="row py-5 my-5 mob-my-0">
		<div class="col-lg-6">
			<div class="position-relative z-2 text-center text-lg-left">
				<p class="mimic-h2">LinkedIn Training</p>
				<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
				<p class="mb-3 text-larger">Do you want increased referrals, appointments and sales using LinkedIn?</p>
				<p class="mb-5 text-larger">We work with start-ups to SMEs and provide marketing expertise, and access to networks, partners and suppliers to enable sustainable growth.</p>
			</div>
		</div>
		<div class="col-lg-6 d-none d-lg-block text-right">
			
			<picture>
				<source srcset="/img/graphics/hands.webp" type="image/webp"/> 
				<source srcset="/img/graphics/hands.jpg" type="image/jpeg"/> 
				<img src="/img/graphics/hands.jpg" type="image/jpeg" alt="The Growth Company LinkedIn Training" width="670" height="510" class="h-auto lazy  hands"/>
			</picture>
		</div>
		<div class="col-12 position-relative z-2">
			<div class="row">
				<div class="col-lg-4">
					<div class="card bg-primary p-3 mob-mb-3 text-center text-lg-left" data-aos="fade-up" data-aos-delay="200">
						<p class="mb-0 text-large line-height-1-3"><b>LinkedIn <br/>Training</b></p>
						<div class="line my-2 py-1 ml-0"><span class="ml-0 mob-mx-auto"></span></div>
						<p class="line-height-1-3">Virtual training designed to enable you to master LinkedIn to get increased referrals, appointments and sales.</p>
						<p class="mb-0"><b><a href="/services/linkedin-training">Read more</a></b><i class="fa fa-angle-double-right ml-3" aria-hidden="true"></i></p>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card bg-primary p-3 mob-mb-3 text-center text-lg-left" data-aos="fade-up" data-aos-delay="300">
						<p class="mb-0 text-large line-height-1-3"><b>Sales Navigator <br/>Training</b></p>
						<div class="line my-2 py-1 ml-0"><span class="ml-0 mob-mx-auto"></span></div>
						<p class="line-height-1-3">Maximise your LinkedIn Sales Navigator investment, engage more prospects and connect with decision makers.</p>
						<p class="mb-0"><b><a href="/services/sales-navigator-training">Read more</a></b><i class="fa fa-angle-double-right ml-3" aria-hidden="true"></i></p>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card bg-primary p-3 mob-mb-3 text-center text-lg-left" data-aos="fade-up" data-aos-delay="400">
						<p class="mb-0 text-large line-height-1-3"><b>Social Selling<br/>Workshops</b></p>
						<div class="line my-2 py-1 ml-0"><span class="ml-0 mob-mx-auto"></span></div>
						<p class="line-height-1-3">Confidently use LinkedIn to prospect better, advance pipeline opportunities and close deals fast.</p>
						<p class="mb-0"><b><a href="/services/social-selling-workshops">Read more</a></b><i class="fa fa-angle-double-right ml-3" aria-hidden="true"></i></p>
					</div>
				</div>
			</div>
		</div>
		<picture>
			<source srcset="/img/graphics/hands.webp" type="image/webp"/> 
			<source srcset="/img/graphics/hands.jpg" type="image/jpeg"/> 
			<img src="/img/graphics/hands.jpg" type="image/jpeg" alt="The Growth Company LinkedIn Training" width="670" height="510" class="h-auto lazy d-lg-none mob-hands"/>
		</picture>
	</div> 
	<div class="row py-5 my-5 position-relative z-2">
		<div class="col-lg-6 pt-5 d-none d-lg-block">
			<picture>
				<source srcset="/img/graphics/socialselling.webp" type="image/webp"/> 
				<source srcset="/img/graphics/socialselling.jpg" type="image/jpeg"/> 
				<img src="/img/graphics/socialselling.jpg" type="image/jpeg" alt="The Growth Company Social Selling Programmes" width="670" height="510" class="h-auto lazy socialselling" style="margin-top: -100px;" />
			</picture>
		</div>
		<div class="col-lg-6 pt-5">
			<div class="position-relative z-2 text-center text-lg-right">
				<p class="mimic-h2">Social Selling Programmes</p>
				<div class="line line-primary my-3 text-center text-lg-right"><span class="mr-0 mob-mx-auto"></span></div>
				<p class="text-larger mb-4">Aimed at entrepreneurs, small business owners, consultants and coaches who want to master social selling to increase referrals, appointments, and sales.</p>

			</div>
			<picture>
				<source srcset="/img/graphics/socialselling.webp" type="image/webp"/> 
				<source srcset="/img/graphics/socialselling.jpg" type="image/jpeg"/> 
				<img src="/img/graphics/socialselling.jpg" type="image/jpeg" alt="The Growth Company Social Selling Programmes" width="670" height="510" class="h-auto lazy d-lg-none mob-socialselling" />
			</picture>
			
		</div>
		<div class="col-12">
			<div class="row">
				<div class="col-lg-4">
					<div class="card bg-primary p-3 mob-mb-3 text-center text-lg-left" data-aos="fade-up" data-aos-delay="500">
						<p class="mb-0 text-large line-height-1-3"><b>Social Selling <br/>Excellence</b></p>
						<div class="line my-2 py-1 ml-0"><span class="ml-0 mob-mx-auto"></span></div>
						<p class="line-height-1-3">For business owners who want to master LinkedIn social selling to increase referrals, appointments & sales.</p>
						<p class="mb-0"><b><a href="/services/social-selling-excellence">Read more</a></b><i class="fa fa-angle-double-right ml-3" aria-hidden="true"></i></p>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card bg-primary p-3 mob-mb-3 text-center text-lg-left" data-aos="fade-up" data-aos-delay="500">
						<p class="mb-0 text-large line-height-1-3"><b>Social Selling <br/>Enterprise</b></p>
						<div class="line my-2 py-1 ml-0"><span class="ml-0 mob-mx-auto"></span></div>
						<p class="line-height-1-3">Live webinars for remote sales teams to confidently use LinkedIn to prospect and advance pipeline opportunities.</p>
						<p class="mb-0"><b><a href="/services/social-selling-enterprise">Read more</a></b><i class="fa fa-angle-double-right ml-3" aria-hidden="true"></i></p>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card bg-primary p-3 mob-mb-3 text-center text-lg-left" data-aos="fade-up" data-aos-delay="500">
						<p class="mb-0 text-large line-height-1-3"><b>Social Selling <br/>Enablement</b></p>
						<div class="line my-2 py-1 ml-0"><span class="ml-0 mob-mx-auto"></span></div>
						<p class="line-height-1-3">A structured enablement programme to help you kickstart Social Selling from within your organisation.</p>
						<p class="mb-0"><b><a href="/services/social-selling-enablement">Read more</a></b><i class="fa fa-angle-double-right ml-3" aria-hidden="true"></i></p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row py-5 position-relative z-1">
		<div class="col-lg-6 py-5 mob-pt-0 ">
			<div class="position-relative z-2 text-center text-lg-left">
				<p class="mimic-h2">Social Media Managed Services</p>
				<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
				<p class="mb-4">Daily social media content planning and becoming a thought leader in your industry can be overwhelming, and worst of all, time-consuming. So let us do the heavy lifting.</p>
				<a href="/services/social-media-managed-services">
					<button type="button" class="btn btn-primary">Learn more</button>
				</a>
			</div>
			<picture>
				<source srcset="/img/graphics/socialmedia.webp" type="image/webp"/> 
				<source srcset="/img/graphics/socialmedia.jpg" type="image/jpeg"/> 
				<img src="/img/graphics/socialmedia.jpg" type="image/jpeg" alt="The Growth Company Strategy" width="670" height="510" class="h-auto lazy d-lg-none mob-socialmedia"/>
			</picture>
			
		</div>
		<div class="col-lg-6 py-5 d-none d-lg-block">
			<picture>
				<source srcset="/img/graphics/socialmedia.webp" type="image/webp"/> 
				<source srcset="/img/graphics/socialmedia.jpg" type="image/jpeg"/> 
				<img src="/img/graphics/socialmedia.jpg" type="image/jpeg" alt="The Growth Company Strategy" width="670" height="510" class="h-auto lazy socialmedia"/>
			</picture>
		</div>
	</div>
</div>
<div class="container-fluid position-relative z-2 py-5 mt-5 mob-px-4">
	<picture>
		<source srcset="/img/graphics/globe-2.webp" type="image/webp"/> 
		<source srcset="/img/graphics/globe-2.png" type="image/png"/> 
		<img src="/img/graphics/globe-2.png" type="image/png" alt="The Growth Company background globe 2" width="1170" height="619" class="lazy bg-left"/>
	</picture>
	<div class="row pt-5 mob-pt-0">
		<div class="col-12 text-center mb-5">
			<p class="mimic-h3 mb-5 pb-4">Don’t just take our word for it…</p>
		</div>
		<testimonials></testimonials>
	</div>
</div>
<seen-enough title='Interested in <span class="text-primary">growing</span> your business?' sentence="Our strategies help businesses strengthen their relationships with their customers online." :link="'/contact'" btntext="Let’s Talk"></seen-enough>
@endsection