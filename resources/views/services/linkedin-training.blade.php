@php
$page = 'Services';
$pagetitle = 'LinkedIn Training - Services - The Growth Company';
$metadescription = "Virtual training designed to enable individuals and teams to master LinkedIn to get increased referrals, appointments and sales.";
$pagetype = 'dark';
$pagename = 'linkedin-training';
$ogimage = 'https://thegrowthcompany.ie/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5 mob-pb-0 mt-5">
	<div class="row mob-pt-5">
		<div class="col-lg-6 text-center text-lg-left">
			<div class="d-table w-100 h-100">
				<div class="d-table-cell align-middle w-100 position-relative z-2">
					<h1>LinkedIn Training</h1>
					<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
					<p class="text-large">Optimise LinkedIn and utilise powerful tools like Sales Navigator to increase business.</p>
					<a href="/contact">
						<button type="button" class="btn btn-primary">Lets Talk</button>
					</a>
				</div>
			</div>
			<picture>
		    	<source srcset="/img/graphics/hands.webp" type="image/webp"/> 
		        <source srcset="/img/graphics/hands.png" type="image/png"/> 
		        <img src="/img/graphics/hands.png" type="image/png" alt="The Growth Company background globe 2" width="670" height="510" class="h-auto lazy mt-5 mob-hands-2 d-lg-none"/>
		    </picture>
		</div>
		<div class="col-lg-6 d-none d-lg-block">
			<picture>
		    	<source srcset="/img/graphics/hands.webp" type="image/webp"/> 
		        <source srcset="/img/graphics/hands.png" type="image/png"/> 
		        <img src="/img/graphics/hands.png" type="image/png" alt="The Growth Company background globe 2" width="670" height="510" class="h-auto lazy mt-5" style="margin-top: -100px;"/>
		    </picture>
  		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mt-5 mob-px-4">
	<div class="row text-center text-lg-left">
		<div class="col-lg-6 pr-5 mob-px-3 mb-5 mob-pt-5 mob-mb-0">
			<p class="mimic-h3">LinkedIn Training Courses</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large mb-5">Our LinkedIn Training Courses cover the basics of setting up and optimising your LinkedIn company page, to more advanced courses on how to use LinkedIn Sales Navigator to prospect and generate sales.</p>
			<p></p>
		</div>
		<div class="col-lg-6 pr-5 mob-px-3 mb-5 mob-pt-5">
			<p class="mimic-h3">LinkedIn Profile Optimisation</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large mb-5">Transform your LinkedIn profile with our tailored 1-2-1 LinkedIn profile optimisation support. We’ll cover profile set-up, content to include and how to optimise your profile to be found by more of the types of people you want to connect with.</p>
		</div>
		<div class="col-lg-6 mb-5 pr-5 mob-px-3">
			<p class="mimic-h3">Market Validation </p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large mb-5">Helping you determine if there's a need for your product in your target market. We help businesses validate ideas and this can enable you to reasonably predict whether people will buy your product or service, and whether your business will thrive. </p>
		</div>
		<div class="col-lg-6 mb-5">
			<p class="mimic-h3">LinkedIn Sales Navigator</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large mb-3">This course will enable your sales teams to become confident in using LinkedIn for prospecting and deepening customer relationships.</p>
			<p class="text-large mb-5">We help organisations create a strong online presence on LinkedIn and develop profitable relationships with decision makers within their target markets.</p>
		</div>
		
	</div>
</div>
<div class="container-fluid position-relative z-2 py-5">
    <picture>
        <source srcset="/img/graphics/globe-2.webp" type="image/webp"/> 
        <source srcset="/img/graphics/globe-2.png" type="image/png"/> 
        <img src="/img/graphics/globe-2.png" type="image/png" alt="The Growth Company background globe 2" width="1170" height="619" class="lazy bg-left"/>
    </picture>
    <div class="row pt-5 mob-pt-0">
        <div class="col-12 text-center mb-5">
            <p class="mimic-h3 mb-5 pb-4">Hear from our clients…</p>
        </div>
        <testimonials></testimonials>
    </div>
</div>
<seen-enough title='Interested in LinkedIn Training & Mentoring?' sentence="Optimise LinkedIn and utilise powerful tools like Sales Navigator to increase business." :link="'/contact'" btntext="Let’s Talk"></seen-enough>
@endsection