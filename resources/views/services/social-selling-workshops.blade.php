@php
$page = 'Services';
$pagetitle = 'Social Selling Workshops - Services - The Growth Company';
$metadescription = "Ideal for inside sales teams who would like to confidently use LinkedIn to prospect better, advance pipeline opportunities and close deals fast.";
$pagetype = 'dark';
$pagename = 'social-selling-workshops';
$ogimage = 'https://thegrowthcompany.ie/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5 mob-pb-0 mt-5">
	<div class="row mob-pt-5">
		<div class="col-lg-6 text-center text-lg-left">
			<div class="d-table w-100 h-100">
				<div class="d-table-cell align-middle w-100 position-relative z-2">
					<h1>Social Selling Workshops</h1>
					<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
					<p class="text-large">Ideal for inside sales teams who would like to confidently use LinkedIn to prospect better, advance pipeline opportunities and close deals fast.</p>
					<a href="/contact">
						<button type="button" class="btn btn-primary">Lets Talk</button>
					</a>
				</div>
			</div>
			<picture>
				<source srcset="/img/graphics/socialselling.webp" type="image/webp"/> 
				<source srcset="/img/graphics/socialselling.png" type="image/png"/> 
				<img src="/img/graphics/socialselling.png" type="image/png" alt="The Growth Company background Social Selling" width="670" height="510" class="h-auto lazy mt-5 mob-socialselling-2 d-lg-none"/>
			</picture>
		</div>
		<div class="col-lg-6 d-none d-lg-block">
			<picture>
				<source srcset="/img/graphics/socialselling.webp" type="image/webp"/> 
				<source srcset="/img/graphics/socialselling.png" type="image/png"/> 
				<img src="/img/graphics/socialselling.png" type="image/png" alt="The Growth Company background Social Selling" width="670" height="510" class="h-auto lazy mt-5" style="margin-top: -100px;"/>
			</picture>
		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mt-5 mob-px-4">
	<div class="row text-center text-lg-left">
		<div class="col-lg-4 mb-5">
			<p class="text-larger line-height-1-3"><b>Social Selling For Competitive Advantage</b></p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="mb-5">Enable your sales team to master social selling to have more conversations with buyers and close deals fast as well as separate you from your competitors and give you a distinct, competitive edge.</p>
		</div>
		<div class="col-lg-4 mb-5">
			<p class="text-larger line-height-1-3"><b>Practical Activities That Build Confidence Online</b></p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="mb-5">Your sales team will gain greater confidence in their ability to effectively engage buyers through LinkedIn and other digital mediums. And, with this newfound confidence, team success and profits will soar.</p>
		</div>
		<div class="col-lg-4 mb-5">
			<p class="text-larger line-height-1-3"><b>Prospect Better With Decision Makers</b></p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="mb-5">Prospect better  with fully optimised and buyer-centric LinkedIn profiles and gain greater confidence to prospect and effectively engage prospects through their social networks.</p>
		</div>
		<div class="col-12">
			<p class="mimic-h3 mb-5">12 Expert Tips To Generating Business Leads With LinkedIn Sales Navigator In This <span class="text-primary">Free Video</span></p>
			<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/499215312?h=1fa69c04da&title=0&byline=0&portrait=0" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div>
		</div>
	</div>
	<div class="row my-5 py-5">
		<div class="col-12">
			<p class="mimic-h2 text-center text-lg-left">Social Selling Workshop</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large text-center text-lg-left">More than half of the world now uses social media. With so many people online, it has become increasingly challenging to reach them early enough in their decision-making process. Suddenly developing relationships with buyers through social networks has become an increasingly critical skill.</p>

			<p class="text-large text-center text-lg-left">Social selling is a way of getting the salesperson back into the buying process to influence the decision-making process early, limit interference from the competition, reduce deal closure times and increase win rates. </p>

			<p class="text-large text-center text-lg-left">Martin and his team will help you define objectives and goals specific to your organisation before doing the workshop so that you can monitor progress and see a real ROI.</p>

			<p class="mimic-h3 mt-5 text-center text-lg-left">Workshops Specifically Tailored To Your Business</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large text-center text-lg-left">All training content is customised to each organisation. It will include individual team audits, competitor and target market analysis. If teams are using Sales Navigator as part of their process, Martin can integrate this into the training. He is a proficient and seasoned Sales Navigator user and can provide sales teams with a live demonstration to show just how powerful this tool can be to target buyers strategically.</p>

			<p class="mimic-h3 mt-5 text-center text-lg-left">Workshops or Webinars – The Choice is Yours</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large text-center text-lg-left">Workshops are practical, fun, highly relevant and results-focused. Your team will leave with fully optimised buyer-centric LinkedIn profiles and have the confidence and skills to engage customers online.</p>

			<p class="text-large text-center text-lg-left">With the ongoing global pandemic the program is also available as a series of live webinars. In these last 12 months, these Social Selling Webinars have become increasingly popular and can be combined with coaching and mentoring sessions. Webinars include plenty of take-away tasks for participants to complete, and it also encourages accountability before the next session in the series.</p>

		</div>
		<div class="col-lg-6 mt-5 pt-5">
			<p class="mimic-h3 mb-4 text-center text-lg-left">Learning<br/>Objectives:</p>
			<ul>
				<li>Creating a mindset shift for Digital Transformation</li>
				<li>Understand the importance of owning your Community</li>
				<li>Develop a powerful buyer-centric LinkedIn profile</li>
				<li>How to grow your LinkedIn network the right way</li>
				<li>Identify opportunities in LinkedIn groups</li>
				<li>Leverage content to shape the buyer journey</li>
				<li>Use technology to your advantage&nbsp;</li>
				<li>Write In-Mails and send connection requests that convert</li>
			</ul>
		</div>
		<div class="col-lg-6 mt-5 pt-5">
			<p class="mimic-h3 mb-4 text-center text-lg-left">Expected<br/>Outcomes:</p>
			<ul>
				<li>Activate a powerful Digital Footprint </li>
				<li>Gain more influence in early-stage buying decisions</li>
				<li>Add value and increase knowledge in the buying process</li>
				<li>Attract qualified sales leads and inbound enquiries</li>
				<li>Master the latest, consistently successful methods including Sales Navigator to reach and engage customers online</li>
				<li>Generate increased referrals, appointments and sales</li>
				<li>Increase LinkedIn Social Selling Index (SSI) Score</li>
			</ul>
		</div>
		<div class="col-lg-6 mt-5 pt-5">
			<p class="mimic-h3 mb-4 text-center text-lg-left">Ideal For Businesses That Want To: </p>
			<ul class="pr-5 mob-px-4">
				<li>Use LinkedIn purposefully to build social trust and create a high-quality community</li>
				<li>Build long-term trust real buyers are looking for in the marketplace</li>
				<li>Develop real influence and authority in your sector and connect with decision-makers </li>
				<li>Strategically target and increase value and add knowledge throughout the buying process</li>
				<li>Scale social selling across the business, including Digital Maturity and investment models, managing risk and privacy online</li>
				<li>See a better ROI for existing Sales Navigator Licences</li>
			</ul>
		</div>
		<div class="col-lg-6 mt-5 pt-5">
			<p class="mimic-h3 mb-4 text-center text-lg-left">Social Selling Workshop</p>
			<p class="text-small text-primary text-center text-lg-left"><i>Click numbers 1-5 to read more</i></p>
			<div class="accordion" id="workshopAccordion">
				<div class="">
					<div id="headingOne">
						<p class=""><b><a class="text-left" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne"><span class="text-primary">1. </span> Setting Yourself Up For Success On LinkedIn <i class="d-none d-lg-inline fa fa-chevron-down ml-3 text-primary"></i></a></b></p>
					</div>
					<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#workshopAccordion">
						<ul class="pl-5">
							<li>Introduction to Social Selling</li>
							<li>Understanding VCCO</li>
							<li>Know Your Target Audience</li>
							<li>Build A All-Star Buyer-Centric LinkedIn Profile</li>
							<li>Understand Privacy and Settings</li>
							<li>How To Find The Right Keywords To Use</li>
							<li>Write A Bio/Summary Section That Converts</li>
							<li>Making The Shift From Resume To Reputation</li>
							<li>Positioning and Messaging For Competitive Advantage</li>
							<li>Add Featured Media and Links To Educate Buyers</li>
							<li>Get High Profile Recommendations </li>
							<li>Strengthen Skills & Endorsements</li>
						</ul>
					</div>
				</div>
				<div class="">
					<div class="" id="headingTwo">
						<p class=""><b><a class="text-left" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><span class="text-primary">2.</span> Prospecting To Build Your Sales Pipeline <i class="d-none d-lg-inline fa fa-chevron-down ml-3 text-primary"></i></a></b></p>
					</div>
					<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#workshopAccordion">
						<div class="">
							<ul class="pl-5">
								<li>Understanding Your Social Selling Index Score (SSI)</li>
								<li>Applying Boolean Search To Find Your Target Market</li>
								<li>Leverage Company Searches To Find Prospects</li>
								<li>Research Prospects and Companies</li>
								<li>Using Data Analytics For Competitive Advantage</li>
								<li>Know Difference Between Lead & Account Searching*</li>
								<li>Saving Leads & Accounts, Lists and Searches*</li>
								<li>Leveraging Sales Spotlight To Segment Data For Better Results*</li>
								<li>Segment Prospects Using Tags & Notes* </li>
							</ul>
							<p class="pl-4 text-small">* LinkedIn Sales Navigator only</p>
						</div>
					</div>
				</div>
				<div class="">
					<div class="" id="headingThree">
						<p class=""><b><a class="text-left" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><span class="text-primary">3.</span> Growing Your LinkedIn Network <i class="d-none d-lg-inline fa fa-chevron-down ml-3 text-primary"></i></a></b></p>
					</div>
					<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#workshopAccordion">
						<div class="">
							<ul class="pl-5">
								<li>Talking To Strangers On LinkedIn</li>
								<li>How To Prepare Before Reaching Out </li>
								<li>Understanding Degree’s of Connections </li>
								<li>Personalising Connection Requests</li>
								<li>Crafting Warm Introductions</li>
								<li>Leveraging Technology For Smart Insights</li>
								<li>Connection Requests Do’s and Don’ts</li>
								<li>Why Not To Connect with Everybody and Anybody </li>
								<li>How to Correctly Use In-Mail*</li>
								<li>Back Up Your Connections</li>
							</ul>
							<p class="pl-4 text-small">* LinkedIn Sales Navigator only</p>
						</div>
					</div>
				</div>
				<div class="">
					<div class="" id="headingFour">
						<p class=""><b><a class="text-left" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour"><span class="text-primary">4.</span> Leveraging Content To Have More Conversations <i class="d-none d-lg-inline fa fa-chevron-down ml-3 text-primary"></i></a></b></p>
					</div>
					<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#workshopAccordion">
						<div class="">
							<ul class="pl-5">
								<li>The Content Mix That Gives You Visibility</li>
								<li>The Importance Of Familiarity </li>
								<li>Posting Relevant and Original Content</li>
								<li>Sharing Success And Why To Do It</li>
								<li>Understanding Hashtags and @Mentions</li>
								<li>Best Times To Reach Target Buyers</li>
								<li>Tools That Automate and Schedule</li>
								<li>Measuring Influence and Amplification </li>
								<li>What’s Not To Post On LinkedIn</li>
								<li>Why Spelling and Grammar Matter</li>
								<li>Leveraging Social Listening Tools As Triggers</li>
								<li>How To Spy On Competitors Without Them Knowing</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="">
					<div class="" id="headingFive">
						<p class=""><b><a class="text-left" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"><span class="text-primary">5.</span> Course Review & LinkedIn Social Selling 5 A Day <i class="d-none d-lg-inline fa fa-chevron-down ml-3 text-primary"></i></a></b></p>
					</div>
					<div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#workshopAccordion">
						<div class="">
							<ul class="pl-5">
								<li>Review Topics and Content Covered In Webinar</li>
								<li>Measuring ROI and Criteria For Success</li>
								<li>LinkedIn 5 A Day Daily Routine</li>
								<li>LinkedIn 5 A Day Weekly Team Challenges</li>
								<li>Questions and Answers with Martin</li>
								<li>LinkedIn Sales Navigator Demo*</li>
							</ul>
							<p class="pl-4 text-small">* LinkedIn Sales Navigator only</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12 mb-4 text-center text-lg-left">
			<p class="mimic-h2 mb-4">Pricing and Delivery Options</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
		</div>
		<div class="col-lg-6 mb-4">
			<div class="card bg-primary px-4 py-5">
				<p class="mimic-h3 mb-0 text-center">Social Selling<br/>Training Workshop</p>
				<p class="text-center">(Full Day or 2 Half Days)</p>
				<p class="text-large text-center mb-0"><b>£4800 per Workshop*</b></p>
				<p class="text-small text-center mb-0"><i>*Excludes Travel Expenses</i></p>
				<div class="line line-white my-4 text-center text-lg-left"><span class="mx-auto"></span></div>
				<ul>
					<li>Max 15 Participants</li>
					<li>Full Day or Two Half Days On Site Training</li>
					<li>Pre-workshop Company, Competitor and Buyer Research</li>
					<li>Recordings Available</li>
					<li>LinkedIn SSI Review (Sales Navigator)</li>
					<li>Training Material Relevant To Company and Industry</li>
					<li>1-Hour One-On-One Mentoring Session Via Zoom For Each Attendee*</li>
				</ul>
				<p class="text-small text-center">* Available at £3100 without Mentoring</p>
				
				<p class="text-larger text-center mt-3 mb-2"><b>Interested?</b></p>
				<a href="/contact" class=" text-center">
					<button type="button" class="btn btn-white mx-auto">Let's Talk</button>
				</a>

			</div>
		</div>
		<div class="col-lg-6 mb-4">
			<div class="card bg-primary px-4 py-5">
				<p class="mimic-h3 mb-0 text-center">Social Selling<br/>Enterprise Webinar Series</p>
				<p class="text-large text-center mb-0"><b>£5700 per Series*</b></p>
				<div class="line line-white my-4 text-center text-lg-left"><span class="mx-auto"></span></div>
				<ul>
					<li>Max 80 Learners*</li>
					<li>Bi-Weekly Instalments (10 Weeks Total)</li>
					<li>5 Live Webinars (1 Hours with active discussion and participation)</li>
					<li>Pre Webinar Company Research</li>
					<li>Delivered over 8 Weeks</li>
					<li>Recordings Available</li>
					<li>LinkedIn SSI Index Score Improvement</li>
					<li>Live Demos and Prospecting</li>
					<li>Individual and Team Accountability after each session</li>
				</ul>				
				<p class="text-larger text-center mt-3 mb-2"><b>Interested?</b></p>
				<a href="/contact" class=" text-center">
					<button type="button" class="btn btn-white mx-auto mb-3 mob-mb-0">Let's Talk</button>
				</a>

			</div>
		</div>
		<div class="col-lg-6 mb-4">
			<div class="card bg-primary px-4 py-5">
				<p class="mimic-h3 mb-0 text-center">Business<br/>Mentoring</p>
				<p class="text-center">3, 6 and 12 Month Mentoring Programmes Available</p>
				<p class="text-large text-center mb-0"><b>£1170 per 3 Months</b></p>
				<div class="line line-white my-4 text-center text-lg-left"><span class="mx-auto"></span></div>
				<ul>
					<li>Up to 4 Hours Support Per Month</li>
					<li>One or Two Meetings As Required</li>
					<li>Over the telephone or Zoom for Clients anywhere in the UK or international.</li>
					<li>Face-to-face available in Belfast and Dublin</li>
					<li>Suitable for Solopreneurs or SMEs regardless of organisation or business type.</li>
					<li>Support can include:</li>
					<li>Social Selling Strategy</li>
					<li>Social Media Profile Optimisation</li>
				</ul>
				<p class="text-small text-center">* Available - 6 Months (£2340) & 12 Months (£4680)</p>
				
				<p class="text-larger text-center mt-3 mb-2"><b>Interested?</b></p>
				<a href="/contact" class=" text-center">
					<button type="button" class="btn btn-white mx-auto">Let's Talk</button>
				</a>

			</div>
		</div>
		<div class="col-lg-6 mb-4">
			<div class="card bg-primary px-4 py-5">
				<p class="mimic-h3 mb-0 text-center">Executive<br/>Coaching</p>
				<p class="text-center">10 - 20 - 30 Hour Coaching Blocks Available</p>
				<p class="text-large text-center mb-0"><b>£2500 Per 10 Sessions</b></p>
				<div class="line line-white my-4 text-center text-lg-left"><span class="mx-auto"></span></div>
				<ul>
					<li>Ten one-hour coaching sessions</li>
					<li>Delivered on Zoom</li>
					<li>Face-to-face available in Belfast & Dublin</li>
					<li>Email correspondence and action points between sessions</li>
					<li>Monthly 15-min action call</li>
					<li>Action-planning to ensure sustainable change</li>
					<li>Suitable for executives or senior decision-makers regardless of organisation size</li>
					<li>Achieving your goals may seem like an uphill struggle, but Martin will push you to the summit of your success.</li>
				</ul>
				<p class="text-small text-center">* Available in larger blocks: - 20 sessions (£4,750) - 30 sessions (£6,750)</p>
				
				<p class="text-larger text-center mt-3 mb-2"><b>Interested?</b></p>
				<a href="/contact" class=" text-center">
					<button type="button" class="btn btn-white mx-auto">Let's Talk</button>
				</a>

			</div>
		</div>
	</div>
</div>
<div class="container-fluid position-relative z-2 py-5">
	<picture>
		<source srcset="/img/graphics/globe-2.webp" type="image/webp"/> 
		<source srcset="/img/graphics/globe-2.png" type="image/png"/> 
		<img src="/img/graphics/globe-2.png" type="image/png" alt="The Growth Company background globe 2" width="1170" height="619" class="lazy bg-left"/>
	</picture>
	<div class="row pt-5 mob-pt-0">
		<div class="col-12 text-center mb-5">
			<p class="mimic-h3 mb-5 pb-4">Hear from our clients…</p>
		</div>
		<testimonials></testimonials>
	</div>
</div>
<seen-enough title='Interested in Social Selling Workshops?' sentence="Master LinkedIn and Social Selling to increase referrals, appointments and sales." :link="'/contact'" btntext="Let’s Talk"></seen-enough>
@endsection
@section('scripts')
<script src="https://player.vimeo.com/api/player.js"></script>
@endsection