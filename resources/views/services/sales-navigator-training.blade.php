@php
$page = 'Services';
$pagetitle = 'Sales Navigator Training - Services - The Growth Company';
$metadescription = "Maximise your LinkedIn Sales Navigator investment, engage more prospects and connect with decision makers.  ";
$pagetype = 'dark';
$pagename = 'sales-navigator-training';
$ogimage = 'https://thegrowthcompany.ie/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5 mob-pb-0 mt-5">
	<div class="row mob-pt-5">
		<div class="col-lg-6 text-center text-lg-left">
			<div class="d-table w-100 h-100">
				<div class="d-table-cell align-middle w-100 position-relative z-2">
					<h1>Sales Navigator Training</h1>
					<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
					<p class="text-large">LinkedIn marketing and social selling consulting to turn contacts into contracts.</p>
					<a href="/contact">
						<button type="button" class="btn btn-primary">Lets Talk</button>
					</a>
				</div>
			</div>
			<picture>
		    	<source srcset="/img/graphics/hands.webp" type="image/webp"/> 
		        <source srcset="/img/graphics/hands.png" type="image/png"/> 
		        <img src="/img/graphics/hands.png" type="image/png" alt="The Growth Company background globe 2" width="670" height="510" class="h-auto lazy mt-5 mob-hands-2 d-lg-none"/>
		    </picture>
		</div>
		<div class="col-lg-6 d-none d-lg-block">
			<picture>
		    	<source srcset="/img/graphics/hands.webp" type="image/webp"/> 
		        <source srcset="/img/graphics/hands.png" type="image/png"/> 
		        <img src="/img/graphics/hands.png" type="image/png" alt="The Growth Company background globe 2" width="670" height="510" class="h-auto lazy mt-5" style="margin-top: -100px;"/>
		    </picture>
  		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mt-5 mob-px-4">
	<div class="row text-center text-lg-left">
		<div class="col-lg-6 pr-5 mob-px-3 mb-5 mob-pt-5">
			<p class="mimic-h3">Title </p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
		
	</div>
</div>
<div class="container-fluid position-relative z-2 py-5">
    <picture>
        <source srcset="/img/graphics/globe-2.webp" type="image/webp"/> 
        <source srcset="/img/graphics/globe-2.png" type="image/png"/> 
        <img src="/img/graphics/globe-2.png" type="image/png" alt="The Growth Company background globe 2" width="1170" height="619" class="lazy bg-left"/>
    </picture>
    <div class="row pt-5 mob-pt-0">
        <div class="col-12 text-center mb-5">
            <p class="mimic-h3 mb-5 pb-4">Hear from our clients…</p>
        </div>
        <testimonials></testimonials>
    </div>
</div>
<seen-enough title='Interested in Sales Navigator Training?' sentence="LinkedIn marketing and social selling consulting to turn contacts into contracts." :link="'/contact'" btntext="Let’s Talk"></seen-enough>
@endsection