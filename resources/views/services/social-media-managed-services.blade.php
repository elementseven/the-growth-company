@php
$page = 'Services';
$pagetitle = 'Social Media Managed Services - Services - The Growth Company';
$metadescription = "Daily social media content planning and becoming a thought leader in your industry can be overwhelming, and worst of all, time-consuming. So let us do the heavy lifting.";
$pagetype = 'dark';
$pagename = 'social-media-managed-services';
$ogimage = 'https://thegrowthcompany.ie/img/og.jpg';
@endphp
@extends('layouts.app', ['pagetitle' => $pagetitle, 'pagetype' => $pagetype, 'pagename' => $pagename, 'metadescription' => $metadescription, 'ogimage' => $ogimage])
@section('header')
<header class="container py-5 mob-pb-0 mt-5">
	<div class="row mob-pt-5">
		<div class="col-lg-6 text-center text-lg-left">
			<div class="d-table w-100 h-100">
				<div class="d-table-cell align-middle w-100 position-relative z-2">
					<h1>Social Media Managed Services</h1>
					<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
					<p class="text-large">No more outdated social media content - we can manage it all for you!</p>
					<a href="/contact">
						<button type="button" class="btn btn-primary">Lets Talk</button>
					</a>
				</div>
			</div>
			<picture>
		    	<source srcset="/img/graphics/socialmedia.webp" type="image/webp"/> 
		        <source srcset="/img/graphics/socialmedia.png" type="image/png"/> 
		        <img src="/img/graphics/socialmedia.png" type="image/png" alt="The Growth Company background globe 2" width="670" height="510" class="h-auto lazy mt-5 mob-socialmedia d-lg-none"/>
		    </picture>
		</div>
		<div class="col-lg-6 d-none d-lg-block">
			<picture>
		    	<source srcset="/img/graphics/socialmedia.webp" type="image/webp"/> 
		        <source srcset="/img/graphics/socialmedia.png" type="image/png"/> 
		        <img src="/img/graphics/socialmedia.png" type="image/png" alt="The Growth Company background globe 2" width="670" height="510" class="h-auto lazy mt-5" style="margin-top: -100px;"/>
		    </picture>
  		</div>
	</div>
</header>
@endsection
@section('content')
<div class="container mt-5 mob-px-4">
	<div class="row text-center text-lg-left">
		<div class="col-lg-6 pr-5 mob-px-3 mb-5 mob-pt-5">
			<p class="mimic-h3">Social Media Strategy</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large mb-5">We will work with you, getting under the skin of your business to help you how to understand how to scale your business for the digital buyer.</p>
		</div>
		<div class="col-lg-6 mb-5">
			<p class="mimic-h3">Content Marketing</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large mb-5">After a discovery and exploration phase, our team will re-invigorate the the look, feel, aim, and direction of your brand’s content strategy (across all social media platforms)</p>
		</div>
		<div class="col-lg-6 pr-5 mob-px-3 mb-5">
			<p class="mimic-h3">Paid Social Campaigns</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large mb-5">To thrive in a “pay-to-play” world, The Growth Company builds, manages, and optimises social media ad campaigns that amplify awareness, consideration, and conversions across each platform.</p>
		</div>
		<div class="col-lg-6 mb-5 mob-mb-0">
			<p class="mimic-h3">Branded Content</p>
			<div class="line line-primary my-3 text-center text-lg-left"><span class="ml-0 mob-mx-auto"></span></div>
			<p class="text-large mb-5">Thumb-stopping graphics, ‘insta-worthy’ photos, and short-form video production. The Growth Company tailors visual content for distribution across all your favourite digital and social platforms.</p>
		</div>
	</div>
</div>
{{-- <div class="container-fluid position-relative z-2 py-5">
    <picture>
        <source srcset="/img/graphics/globe-2.webp" type="image/webp"/> 
        <source srcset="/img/graphics/globe-2.png" type="image/png"/> 
        <img src="/img/graphics/globe-2.png" type="image/png" alt="The Growth Company background globe 2" width="1170" height="619" class="lazy bg-left"/>
    </picture>
    <div class="row pt-5 mob-pt-0">
        <div class="col-12 text-center mb-5">
            <p class="mimic-h3 mb-5 pb-4">Hear from our clients…</p>
        </div>
        <testimonials></testimonials>
    </div>
</div> --}}
<seen-enough title='Interested in Social Media Managed Services?' sentence="No more outdated social media content - we can manage it all for you!" :link="'/contact'" btntext="Let’s Talk"></seen-enough>
@endsection