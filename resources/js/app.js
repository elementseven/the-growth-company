/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require("babel-polyfill");
require("whatwg-fetch");
require('./bootstrap');
window.LazyLoad = require('vanilla-lazyload');
import Vue from 'vue';
require('./plugins/modernizr-custom.js');
require('waypoints/lib/jquery.waypoints.min.js');
import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload);
require('./plugins/cookieConsent.js');

window.AOS = require('AOS');
AOS.init({ offset: (window.innerHeight * 0) });
window.addEventListener('load', AOS.refresh);
window.addEventListener('resize', AOS.refresh);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

Vue.component('main-menu', () => import(/* webpackChunkName: "MainMenu" */ './components/Menus/MainMenu.vue'));
Vue.component('mobile-menu', () => import(/* webpackChunkName: "MobileMenu" */ './components/Menus/MobileMenu.vue'));
Vue.component('site-footer', () => import(/* webpackChunkName: "Footer" */ './components/Footer.vue'));

Vue.component('testimonials', () => import(/* webpackChunkName: "Testimonials" */ './components/Testimonials.vue'));
Vue.component('mailing-list', () => import(/* webpackChunkName: "MailingList" */ './components/MailingList.vue'));
Vue.component('loader', () => import(/* webpackChunkName: "Loader" */ './components/Loader.vue'));

Vue.component('contact-page-form', () => import(/* webpackChunkName: "ContactPageForm" */ './components/Contact/ContactPageForm.vue'));

Vue.component('home-header', require(/* webpackChunkName: "HomeHeader" */ './components/Home/HomeHeader.vue').default);
Vue.component('services-carousel', require(/* webpackChunkName: "ServicesCarousel" */ './components/Home/ServicesCarousel.vue').default);
Vue.component('client-logos', () => import(/* webpackChunkName: "ClientLogos" */ './components/ClientLogos.vue'));

Vue.component('news-index', () => import(/* webpackChunkName: "NewsIndex" */ './components/news/Index.vue'));
Vue.component('news-show', () => import(/* webpackChunkName: "NewsShow" */ './components/news/Show.vue'));
Vue.component('news-inline', () => import(/* webpackChunkName: "NewsShow" */ './components/news/Inline.vue'));

Vue.component('case-studies-index', () => import(/* webpackChunkName: "CaseStudiesIndex" */ './components/CaseStudies/Index.vue'));
Vue.component('case-studies-show', () => import(/* webpackChunkName: "CaseStudiesShow" */ './components/CaseStudies/Show.vue'));

Vue.component('seen-enough', () => import(/* webpackChunkName: "SeenEnough" */ './components/contact/SeenEnough.vue'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
