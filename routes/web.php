<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PageController::class, 'home'])->name('home');
Route::get('/services', [PageController::class, 'services'])->name('servcies');
Route::get('/services/sales-navigator-training', [PageController::class, 'salesNavigatorTraining'])->name('servcies.sales-navigator-training');
Route::get('/services/linkedin-training', [PageController::class, 'linkedinTraining'])->name('servcies.linkedin-training');
Route::get('/services/social-selling-workshops', [PageController::class, 'socialSellingWorkshops'])->name('servcies.social-selling-workshops');
Route::get('/services/social-selling-excellence', [PageController::class, 'socialSellingExcellence'])->name('servcies.social-selling-excellence');
Route::get('/services/social-selling-enterprise', [PageController::class, 'socialSellingEnterprise'])->name('servcies.social-selling-enterprise');
Route::get('/services/social-selling-enablement', [PageController::class, 'socialSellingEnablement'])->name('servcies.social-selling-enablement');
Route::get('/services/social-media-managed-services', [PageController::class, 'socialMediaManagedServices'])->name('servcies.social-media-managed-services');

Route::get('/about', [PageController::class, 'about'])->name('about');
Route::get('/contact', [PageController::class, 'contact'])->name('contact');
Route::get('/terms-and-conditions', [PageController::class, 'tandcs'])->name('tandcs');
Route::get('/privacy-policy', [PageController::class, 'privacyPolicy'])->name('privacy-policy');

Route::get('/case-studies', [CaseStudiesController::class, 'index'])->name('case-studies.index');
Route::get('/case-studies/get', [CaseStudiesController::class, 'get'])->name('case-studies.get');
Route::get('/case-studies/{slug}', [CaseStudiesController::class, 'show'])->name('case-studies.show');

// News Routes
Route::get('/blog', [NewsController::class, 'index'])->name('news.index');
Route::get('/news/get', [NewsController::class, 'get'])->name('news.get');
Route::get('/blog/{date}/{slug}', [NewsController::class, 'show'])->name('news.show');

Route::post('/send-message', [SendMail::class, 'enquiry'])->name('send-message');
Route::post('/mailinglist', [SendMail::class, 'mailingListSignup'])->name('mailing-list');