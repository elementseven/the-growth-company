<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','slug'
    ];
    protected static function boot()
    {
        parent::boot();
        static::saving(function ($category) {
            $category->slug = Str::slug($category->name, "-");
        });
    }

    public function posts(){
        return $this->belongsToMany('App\Models\Post', 'category_post')->withPivot('post_id');
    }

    public function casestudies(){
        return $this->belongsToMany('App\Models\CaseStudy', 'case_study_category')->withPivot('case_study_id');
    }
}
