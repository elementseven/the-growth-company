<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home(){
        return view('home');
    }
    public function about(){
        return view('about');
    }
    public function contact(){
        return view('contact');
    }
    public function services(){
        return view('services.index');
    }
    public function salesNavigatorTraining(){
        return view('services.sales-navigator-training');
    }
    public function linkedinTraining(){
        return view('services.linkedin-training');
    }
    public function socialSellingWorkshops(){
        return view('services.social-selling-workshops');
    }
    public function socialSellingExcellence(){
        return view('services.social-selling-excellence');
    }
    public function socialSellingEnterprise(){
        return view('services.social-selling-enterprise');
    }
    public function socialSellingEnablement(){
        return view('services.social-selling-enablement');
    }
    public function socialMediaManagedServices(){
        return view('services.social-media-managed-services');
    }
    public function caseStudies(){
        return view('case-studies.index');
    }
    public function tandcs(){
        return view('tandcs');
    }
    public function privacyPolicy(){
        return view('privacy-policy');
    }
}