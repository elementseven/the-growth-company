<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\CaseStudy;

class CaseStudiesController extends Controller
{
  // Main case studies page
  public function index(){
    $categories = Category::has('casestudies')->orderBy('name','asc')->get();
    return view('case-studies.index')->with(['categories' => $categories]);
  }

  // Return json case studies
  public function get(Request $request){
    if($request->input('category') == '*'){
      $posts = CaseStudy::where('status','!=','draft')
      ->orderBy('created_at','desc')
      ->with('categories')
      ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);
    }else{
      $posts = CaseStudy::orderBy('created_at','desc')->whereHas('categories', function($q) use($request){
          $q->where('id', $request->input('category'));
      })
      ->where('status','!=','draft')
      ->orderBy('created_at','desc')
      ->with('categories')
      ->paginate($request->input('limit'),['id','title', 'slug', 'excerpt','created_at']);
    }
    foreach($posts as $p){
      $p->normal = $p->getFirstMediaUrl('casestudies', 'normal');
      $p->normalwebp = $p->getFirstMediaUrl('casestudies', 'normal-webp');
      $p->double = $p->getFirstMediaUrl('casestudies', 'double');
      $p->doublewebp = $p->getFirstMediaUrl('casestudies', 'double-webp');
      $p->featured = $p->getFirstMediaUrl('casestudies', 'featured');
      $p->featuredwebp = $p->getFirstMediaUrl('casestudies', 'featured-webp');
      $p->mimetype = $p->getFirstMedia('casestudies')->mime_type;
    }
    return $posts;
  }

  // Single Case Study 
  public function show( $slug){
    $post = CaseStudy::where([['slug',$slug],['status','!=','draft']])->first();
    return view('case-studies.show')->with(['post' => $post]);
  }
}
