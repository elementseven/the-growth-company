<?php

namespace App\Policies;

use App\Models\User;
use App\Models\CaseStudy;
use Illuminate\Auth\Access\HandlesAuthorization;

class CaseStudyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user can create a case study.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CaseStudy  $casestudy
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role->name != 'Guide';
    }

    /**
     * Determine whether the user can update the case study.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CaseStudy  $casestudy
     * @return mixed
     */
    public function update(User $user, CaseStudy $casestudy)
    {
        return $user->role->name != 'Guide';
    }

    /**
     * Determine whether the user can view the cases tudy.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CaseStudy  $casestudy
     * @return mixed
     */
    public function view(User $user, CaseStudy $casestudy)
    {
        return $user->role->name != 'Guide';
    }

    /**
     * Determine whether the user can view the case study.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\CaseStudy  $casestudy
     * @return mixed
     */
    public function delete(User $user, CaseStudy $casestudy)
    {
        return $user->role->name != 'Guide';
    }

    

    
}
