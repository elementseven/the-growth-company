(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["MainMenu"],{

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MainMenu.vue?vue&type=script&lang=js&":



function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusMainMenuVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
props:{
pagetype:String},

mounted:function mounted(){
$(document).ready(function(){
$('#menu-trigger').each(function(index,element){
var inview=new Waypoint({
element:element,
handler:function handler(direction){
if(direction==="down"){
$('#scroll-menu').addClass('on');
}else {
$('#scroll-menu').removeClass('on');
}
},
offset:'0%'});

});
var lazyLoadInstance=new LazyLoad({
elements_selector:".lazy"});

$(".scroll-btn").click(function(){
$('html,body').animate({
scrollTop:$(".scrollTo").offset().top});

});
});
}};


/***/},

/***/"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MainMenu.vue?vue&type=style&index=0&lang=scss&":



function node_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusMainMenuVueVueTypeStyleIndex0LangScss(module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ../../../../node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js */"./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___=_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1];});
// Module
___CSS_LOADER_EXPORT___.push([module.id,".menu-holder {\n  position: relative;\n  z-index: 100;\n}\n#menu-trigger {\n  position: absolute;\n  top: 110vh;\n  left: 0;\n  width: 1px;\n  height: 1px;\n}\n#main-menu-top {\n  z-index: 250;\n}\n#main-menu {\n  position: absolute;\n  top: 0;\n  left: 0;\n}\n.menu {\n  position: absolute;\n  top: 0;\n  left: 0;\n  z-index: 100;\n  width: 100%;\n}\n.menu .menu_logo {\n  width: 220px;\n}\n.menu .menu_logo.menu_logo_normal {\n  width: 220px;\n}\n.menu .menu-links .menu-item {\n  display: inline-block;\n  margin-left: 2.5rem;\n  cursor: pointer;\n  color: #fff;\n  font-family: roboto, sans-serif;\n  font-weight: 700;\n  letter-spacing: 1px;\n  font-size: 1rem;\n  transition: all 300ms ease;\n}\n.menu .menu-links .menu-item a {\n  color: #fff;\n}\n.menu .menu-links .menu-item a:hover {\n  color: #0082B9;\n}\n.menu .menu-links .menu-item i {\n  font-size: 1.2rem;\n  margin-left: 10px;\n}\n.menu .menu-links .menu-item:hover, .menu .menu-links .menu-item.active {\n  color: #0082B9;\n}\n.menu .menu-top-links .menu-links .menu-item {\n  font-size: 0.8rem;\n}\n.menu.opacity {\n  opacity: 0;\n}\n#scroll-menu {\n  background-color: #040F17;\n  position: fixed;\n  top: -500px;\n  left: 0;\n  z-index: 200;\n  width: 100%;\n  box-shadow: 0rem 0rem 1rem rgba(0, 130, 184, 0.1) !important;\n  transition: all 300ms ease;\n}\n#scroll-menu .menu-links .menu-item {\n  color: #fff;\n}\n#scroll-menu .col-lg-10 {\n  margin-top: 19px !important;\n}\n#scroll-menu .menu_logo {\n  width: 170px;\n}\n#scroll-menu.on {\n  top: 0;\n}\n@media only screen and (max-width: 1230px) {\n.menu .menu-links .menu-item {\n    margin-left: 1.5rem;\n    font-size: 0.8rem;\n}\n}\n@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {\n#scroll-menu .menu-links .menu-item:hover,\n  .menu .menu-links .menu-item:hover {\n    box-shadow: none;\n}\n#scroll-menu .menu_logo,\n  .menu .menu_logo {\n    width: 140px;\n    margin-top: 15px !important;\n}\n#menu_btn {\n    display: block;\n}\n}\n@media only screen and (max-width: 767px) {\n.menu .menu_logo {\n    width: 170px;\n}\n}\n",""]);
// Exports
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=___CSS_LOADER_EXPORT___;


/***/},

/***/"./node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MainMenu.vue?vue&type=style&index=0&lang=scss&":



function node_modulesLaravelMixNode_modulesStyleLoaderDistCjsJsNode_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusMainMenuVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! !../../../../node_modules/laravel-mix/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */"./node_modules/laravel-mix/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! !!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./MainMenu.vue?vue&type=style&index=0&lang=scss& */"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MainMenu.vue?vue&type=style&index=0&lang=scss&");



var options={};

options.insert="head";
options.singleton=false;

var update=_node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"],options);



/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"].locals||{};

/***/},

/***/"./resources/js/components/Menus/MainMenu.vue":



function resourcesJsComponentsMenusMainMenuVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _MainMenu_vue_vue_type_template_id_6ce2353e___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./MainMenu.vue?vue&type=template&id=6ce2353e& */"./resources/js/components/Menus/MainMenu.vue?vue&type=template&id=6ce2353e&");
/* harmony import */var _MainMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./MainMenu.vue?vue&type=script&lang=js& */"./resources/js/components/Menus/MainMenu.vue?vue&type=script&lang=js&");
/* harmony import */var _MainMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! ./MainMenu.vue?vue&type=style&index=0&lang=scss& */"./resources/js/components/Menus/MainMenu.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__=__webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");


/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
_MainMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
_MainMenu_vue_vue_type_template_id_6ce2353e___WEBPACK_IMPORTED_MODULE_0__.render,
_MainMenu_vue_vue_type_template_id_6ce2353e___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/Menus/MainMenu.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/Menus/MainMenu.vue?vue&type=script&lang=js&":



function resourcesJsComponentsMenusMainMenuVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./MainMenu.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MainMenu.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"];

/***/},

/***/"./resources/js/components/Menus/MainMenu.vue?vue&type=style&index=0&lang=scss&":



function resourcesJsComponentsMenusMainMenuVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_cjs_js_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!../../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./MainMenu.vue?vue&type=style&index=0&lang=scss& */"./node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MainMenu.vue?vue&type=style&index=0&lang=scss&");


/***/},

/***/"./resources/js/components/Menus/MainMenu.vue?vue&type=template&id=6ce2353e&":



function resourcesJsComponentsMenusMainMenuVueVueTypeTemplateId6ce2353e(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_template_id_6ce2353e___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_template_id_6ce2353e___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_MainMenu_vue_vue_type_template_id_6ce2353e___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./MainMenu.vue?vue&type=template&id=6ce2353e& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MainMenu.vue?vue&type=template&id=6ce2353e&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Menus/MainMenu.vue?vue&type=template&id=6ce2353e&":



function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsMenusMainMenuVueVueTypeTemplateId6ce2353e(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _vm._m(0);
};
var _staticRenderFns=[
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",[
_c("div",{staticClass:"menu",attrs:{id:"main-menu"}},[
_c("div",{staticClass:"container-fluid px-5 mob-px-3 ipadp-px-3"},[
_c("div",{staticClass:"row"},[
_c("div",{staticClass:"col-lg-2 py-3 page-home-pt-0"},[
_c("a",{attrs:{href:"/"}},[
_c("img",{
staticClass:"menu_logo mt-2 mob-mt-0",
attrs:{
src:"/img/logos/logo.svg",
alt:"The Growth Company Logo",
width:"276",
height:"64"}})])]),




_vm._v(" "),
_c(
"div",
{
staticClass:"col-lg-10 text-right d-none d-lg-block mt-4 pt-2"},

[
_c("div",{staticClass:"menu-links d-inline-block"},[
_c(
"a",
{
staticClass:"menu-item cursor-pointer",
attrs:{href:"/services"}},

[_vm._v("Services")])]),


_vm._v(" "),
_c("div",{staticClass:"menu-links d-inline-block "},[
_c(
"a",
{
staticClass:"menu-item cursor-pointer",
attrs:{href:"/case-studies"}},

[_vm._v("Case Studies")])]),


_vm._v(" "),
_c("div",{staticClass:"menu-links d-inline-block"},[
_c(
"a",
{
staticClass:"menu-item cursor-pointer",
attrs:{href:"/about"}},

[_vm._v("About")])]),


_vm._v(" "),
_c("div",{staticClass:"menu-links d-inline-block "},[
_c(
"a",
{
staticClass:"menu-item  cursor-pointer",
attrs:{href:"/blog"}},

[_vm._v("Blog")])]),


_vm._v(" "),
_c("div",{staticClass:"menu-links d-inline-block"},[
_c(
"a",
{
staticClass:"menu-item cursor-pointer",
attrs:{href:"/contact"}},

[_vm._v("Contact")])])])])])]),







_vm._v(" "),
_c(
"div",
{staticClass:"menu d-none d-lg-block",attrs:{id:"scroll-menu"}},
[
_c("div",{staticClass:"container-fluid px-5"},[
_c("div",{staticClass:"row"},[
_c("div",{staticClass:"col-lg-2 py-3"},[
_c("a",{attrs:{href:"/"}},[
_c("img",{
staticClass:"menu_logo",
attrs:{
src:"/img/logos/logo.svg",
alt:"The Growth Company Logo",
width:"276",
height:"64"}})])]),




_vm._v(" "),
_c(
"div",
{staticClass:"col-lg-10 text-right d-none d-lg-block py-3"},
[
_c("div",{staticClass:"menu-links d-inline-block "},[
_c(
"a",
{
staticClass:"menu-item cursor-pointer",
attrs:{href:"/services"}},

[_vm._v("Services")])]),


_vm._v(" "),
_c("div",{staticClass:"menu-links d-inline-block "},[
_c(
"a",
{
staticClass:"menu-item cursor-pointer",
attrs:{href:"/case-studies"}},

[_vm._v("Case Studies")])]),


_vm._v(" "),
_c("div",{staticClass:"menu-links d-inline-block"},[
_c(
"a",
{
staticClass:"menu-item cursor-pointer",
attrs:{href:"/about"}},

[_vm._v("About")])]),


_vm._v(" "),
_c("div",{staticClass:"menu-links d-inline-block "},[
_c(
"a",
{
staticClass:"menu-item  cursor-pointer",
attrs:{href:"/blog"}},

[_vm._v("Blog")])]),


_vm._v(" "),
_c("div",{staticClass:"menu-links d-inline-block"},[
_c(
"a",
{
staticClass:"menu-item cursor-pointer",
attrs:{href:"/contact"}},

[_vm._v("Contact")])])])])])])]);









}];

_render._withStripped=true;



/***/}}]);

}());
