(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["Testimonials"],{

/***/"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Testimonials.vue?vue&type=script&lang=js&":



function node_modulesBabelLoaderLibIndexJsClonedRuleSet50Rules0Use0Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsTestimonialsVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! vue-owl-carousel */"./node_modules/vue-owl-carousel/dist/vue-owl-carousel.js");
/* harmony import */var vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__={
components:{
carousel:vue_owl_carousel__WEBPACK_IMPORTED_MODULE_0___default()},

data:function data(){
return {
errors:{},
success:false,
loaded:true};

}};


/***/},

/***/"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Testimonials.vue?vue&type=style&index=0&lang=scss&":



function node_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsTestimonialsVueVueTypeStyleIndex0LangScss(module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ../../../node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js */"./node_modules/laravel-mix/node_modules/css-loader/dist/runtime/api.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___=_node_modules_laravel_mix_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1];});
// Module
___CSS_LOADER_EXPORT___.push([module.id,".testimonial-card img {\n  margin-top: -100px;\n}\n.testimonial-card a:hover {\n  color: #0082B9;\n}\n#testimonials-carousel .card {\n  position: relative;\n  width: calc(100vw - 3rem);\n}\n#testimonials-carousel .card img {\n  width: 136px;\n}\n#testimonials-carousel .owl-carousel .owl-stage-outer {\n  overflow: visible !important;\n}\n#testimonials-carousel .owl-theme .owl-dots {\n  width: 170px;\n  position: absolute;\n  bottom: -4rem;\n  left: calc(50% - 85px);\n  text-align: center;\n}\n#testimonials-carousel .owl-theme .owl-dots .owl-dot span {\n  width: 30px;\n  height: 8px;\n  background: transparent;\n  border-radius: 0;\n  margin: 5px 2px;\n  border-bottom: 4px solid #fff;\n}\n#testimonials-carousel .owl-theme .owl-dots .owl-dot.active span {\n  border-bottom: 4px solid #0082B9;\n}\n",""]);
// Exports
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=___CSS_LOADER_EXPORT___;


/***/},

/***/"./node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Testimonials.vue?vue&type=style&index=0&lang=scss&":



function node_modulesLaravelMixNode_modulesStyleLoaderDistCjsJsNode_modulesLaravelMixNode_modulesCssLoaderDistCjsJsClonedRuleSet120Rules0Use1Node_modulesVueLoaderLibLoadersStylePostLoaderJsNode_modulesLaravelMixNode_modulesPostcssLoaderDistCjsJsClonedRuleSet120Rules0Use2Node_modulesSassLoaderDistCjsJsClonedRuleSet120Rules0Use3Node_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsTestimonialsVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! !../../../node_modules/laravel-mix/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */"./node_modules/laravel-mix/node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default=/*#__PURE__*/__webpack_require__.n(_node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */var _node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Testimonials_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! !!../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Testimonials.vue?vue&type=style&index=0&lang=scss& */"./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Testimonials.vue?vue&type=style&index=0&lang=scss&");



var options={};

options.insert="head";
options.singleton=false;

var update=_node_modules_laravel_mix_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Testimonials_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"],options);



/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Testimonials_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_1__["default"].locals||{};

/***/},

/***/"./resources/js/components/Testimonials.vue":



function resourcesJsComponentsTestimonialsVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _Testimonials_vue_vue_type_template_id_3251e99f___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./Testimonials.vue?vue&type=template&id=3251e99f& */"./resources/js/components/Testimonials.vue?vue&type=template&id=3251e99f&");
/* harmony import */var _Testimonials_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! ./Testimonials.vue?vue&type=script&lang=js& */"./resources/js/components/Testimonials.vue?vue&type=script&lang=js&");
/* harmony import */var _Testimonials_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__=__webpack_require__(/*! ./Testimonials.vue?vue&type=style&index=0&lang=scss& */"./resources/js/components/Testimonials.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__=__webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");


/* normalize component */

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
_Testimonials_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
_Testimonials_vue_vue_type_template_id_3251e99f___WEBPACK_IMPORTED_MODULE_0__.render,
_Testimonials_vue_vue_type_template_id_3251e99f___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/Testimonials.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/Testimonials.vue?vue&type=script&lang=js&":



function resourcesJsComponentsTestimonialsVueVueTypeScriptLangJs(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Testimonials_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Testimonials.vue?vue&type=script&lang=js& */"./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Testimonials.vue?vue&type=script&lang=js&");
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Testimonials_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"];

/***/},

/***/"./resources/js/components/Testimonials.vue?vue&type=style&index=0&lang=scss&":



function resourcesJsComponentsTestimonialsVueVueTypeStyleIndex0LangScss(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony import */var _node_modules_laravel_mix_node_modules_style_loader_dist_cjs_js_node_modules_laravel_mix_node_modules_css_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_laravel_mix_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_2_node_modules_sass_loader_dist_cjs_js_clonedRuleSet_12_0_rules_0_use_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Testimonials_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!../../../node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!../../../node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Testimonials.vue?vue&type=style&index=0&lang=scss& */"./node_modules/laravel-mix/node_modules/style-loader/dist/cjs.js!./node_modules/laravel-mix/node_modules/css-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/laravel-mix/node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[2]!./node_modules/sass-loader/dist/cjs.js??clonedRuleSet-12[0].rules[0].use[3]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Testimonials.vue?vue&type=style&index=0&lang=scss&");


/***/},

/***/"./resources/js/components/Testimonials.vue?vue&type=template&id=3251e99f&":



function resourcesJsComponentsTestimonialsVueVueTypeTemplateId3251e99f(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Testimonials_vue_vue_type_template_id_3251e99f___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Testimonials_vue_vue_type_template_id_3251e99f___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Testimonials_vue_vue_type_template_id_3251e99f___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Testimonials.vue?vue&type=template&id=3251e99f& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Testimonials.vue?vue&type=template&id=3251e99f&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Testimonials.vue?vue&type=template&id=3251e99f&":



function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsTestimonialsVueVueTypeTemplateId3251e99f(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",{staticClass:"container"},[
_vm._m(0),
_vm._v(" "),
_c("div",{staticClass:"row d-lg-none"},[
_c(
"div",
{staticClass:"col-12",attrs:{id:"testimonials-carousel"}},
[
_c(
"carousel",
{
attrs:{
items:1,
margin:15,
center:false,
autoHeight:true,
autoWidth:true,
nav:false,
dots:true}},


[
_c(
"div",
{
staticClass:
"card testimonial-card bg-primary p-4 text-center"},

[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/testimonials/petergregory.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/testimonials/petergregory.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy mx-auto",
attrs:{
src:"/img/testimonials/petergregory.png",
type:"image/png",
alt:
"Peter Gregory - The Growth Company testimonial image",
width:"136",
height:"136"}})]),



_vm._v(" "),
_c("p",{staticClass:"text-large mb-0 mt-3"},[
_c("b",[_vm._v("Peter Gregory")])]),

_vm._v(" "),
_c("div",{staticClass:"line my-2 py-1"},[
_c("span",{staticClass:"mx-auto"})]),

_vm._v(" "),
_c("p",{staticClass:"text-small mb-2"},[
_vm._v(
"If you want a LinkedIn expert who will give you RESULTS rather than “how many people are in my network” ego boost then you need to speak to Martin.")]),


_vm._v(" "),
_c("p",{staticClass:"mb-0"},[
_c(
"a",
{
staticClass:"text-white",
attrs:{
href:
"https://www.linkedin.com/in/peteradriangregory",
target:"_blank",
"aria-label":"LinkedIn",
rel:"noreferrer"}},


[
_c("i",{staticClass:"fa fa-linkedin-square mr-2"}),
_vm._v(" @peteradriangregory")])])]),





_vm._v(" "),
_c(
"div",
{
staticClass:
"card testimonial-card bg-primary p-4 text-center"},

[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/testimonials/brendanmurphy.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/testimonials/brendanmurphy.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy mx-auto",
attrs:{
src:"/img/testimonials/brendanmurphy.png",
type:"image/png",
alt:
"Brendan Murphy - The Growth Company testimonial image",
width:"136",
height:"136"}})]),



_vm._v(" "),
_c("p",{staticClass:"text-large mb-0 mt-3"},[
_c("b",[_vm._v("Brendan Murphy")])]),

_vm._v(" "),
_c("div",{staticClass:"line my-2 py-1"},[
_c("span",{staticClass:"mx-auto"})]),

_vm._v(" "),
_c("p",{staticClass:"text-small mb-2"},[
_vm._v(
"I feel one always needs to sharpen their social media pencil, and no one better than Martin to help. His no nonsense, straight to the point approach is a breath of fresh air.")]),


_vm._v(" "),
_c("p",{staticClass:"mb-0"},[
_c(
"a",
{
staticClass:"text-white",
attrs:{
href:"https://www.linkedin.com/in/brendanmurph",
target:"_blank",
"aria-label":"LinkedIn",
rel:"noreferrer"}},


[
_c("i",{staticClass:"fa fa-linkedin-square mr-2"}),
_vm._v(" @brendanmurph")])])]),





_vm._v(" "),
_c(
"div",
{
staticClass:
"card testimonial-card bg-primary p-4 text-center"},

[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/testimonials/czarina-sheikh-mathew.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/testimonials/czarina-sheikh-mathew.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy mx-auto",
attrs:{
src:"/img/testimonials/czarina-sheikh-mathew.png",
type:"image/png",
alt:
"Czarina Sheikh Mathew - The Growth Company testimonial image",
width:"136",
height:"136"}})]),



_vm._v(" "),
_c("p",{staticClass:"text-large mb-0 mt-3"},[
_c("b",[_vm._v("Czarina Sheikh Mathew")])]),

_vm._v(" "),
_c("div",{staticClass:"line my-2 py-1"},[
_c("span",{staticClass:"mx-auto"})]),

_vm._v(" "),
_c("p",{staticClass:"text-small mb-2"},[
_vm._v(
"It was refreshing to learn from an expert who so generously shares insights and provides bespoke recommendations on the latest tools to improve your online presence.")]),


_vm._v(" "),
_c("p",{staticClass:"mb-0"},[
_c(
"a",
{
staticClass:"text-white",
attrs:{
href:
"https://uk.linkedin.com/in/czarina-sheikh-mathew",
target:"_blank",
"aria-label":"LinkedIn",
rel:"noreferrer"}},


[
_c("i",{staticClass:"fa fa-linkedin-square mr-2"}),
_vm._v(" @czarina-sheikh-mathew")])])])])],








1)])]);



};
var _staticRenderFns=[
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",{staticClass:"row d-none d-lg-flex"},[
_c("div",{staticClass:"col-lg-4"},[
_c(
"div",
{staticClass:"card testimonial-card bg-primary p-4 text-center"},
[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/testimonials/petergregory.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/testimonials/petergregory.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy",
attrs:{
src:"/img/testimonials/petergregory.png",
type:"image/png",
alt:"Peter Gregory - The Growth Company testimonial image",
width:"136",
height:"136"}})]),



_vm._v(" "),
_c("p",{staticClass:"text-large mb-0 mt-3"},[
_c("b",[_vm._v("Peter Gregory")])]),

_vm._v(" "),
_c("div",{staticClass:"line my-2 py-1"},[
_c("span",{staticClass:"mx-auto"})]),

_vm._v(" "),
_c("p",{staticClass:"text-small mb-2"},[
_vm._v(
"If you want a LinkedIn expert who will give you RESULTS rather than “how many people are in my network” ego boost then you need to speak to Martin.")]),


_vm._v(" "),
_c("p",{staticClass:"mb-0"},[
_c(
"a",
{
staticClass:"text-white",
attrs:{
href:"https://www.linkedin.com/in/peteradriangregory",
target:"_blank",
"aria-label":"LinkedIn",
rel:"noreferrer"}},


[
_c("i",{staticClass:"fa fa-linkedin-square mr-2"}),
_vm._v(" @peteradriangregory")])])])]),






_vm._v(" "),
_c("div",{staticClass:"col-lg-4"},[
_c(
"div",
{staticClass:"card testimonial-card bg-primary p-4 text-center"},
[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/testimonials/brendanmurphy.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/testimonials/brendanmurphy.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy",
attrs:{
src:"/img/testimonials/brendanmurphy.png",
type:"image/png",
alt:"Brendan Murphy - The Growth Company testimonial image",
width:"136",
height:"136"}})]),



_vm._v(" "),
_c("p",{staticClass:"text-large mb-0 mt-3"},[
_c("b",[_vm._v("Brendan Murphy")])]),

_vm._v(" "),
_c("div",{staticClass:"line my-2 py-1"},[
_c("span",{staticClass:"mx-auto"})]),

_vm._v(" "),
_c("p",{staticClass:"text-small mb-2"},[
_vm._v(
"I feel one always needs to sharpen their social media pencil, and no one better than Martin to help. His no nonsense, straight to the point approach is a breath of fresh air.")]),


_vm._v(" "),
_c("p",{staticClass:"mb-0"},[
_c(
"a",
{
staticClass:"text-white",
attrs:{
href:"https://www.linkedin.com/in/brendanmurph",
target:"_blank",
"aria-label":"LinkedIn",
rel:"noreferrer"}},


[
_c("i",{staticClass:"fa fa-linkedin-square mr-2"}),
_vm._v(" @brendanmurph")])])])]),






_vm._v(" "),
_c("div",{staticClass:"col-lg-4"},[
_c(
"div",
{staticClass:"card testimonial-card bg-primary p-4 text-center"},
[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/testimonials/czarina-sheikh-mathew.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/testimonials/czarina-sheikh-mathew.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy",
attrs:{
src:"/img/testimonials/czarina-sheikh-mathew.png",
type:"image/png",
alt:
"Czarina Sheikh Mathew - The Growth Company testimonial image",
width:"136",
height:"136"}})]),



_vm._v(" "),
_c("p",{staticClass:"text-large mb-0 mt-3"},[
_c("b",[_vm._v("Czarina Sheikh Mathew")])]),

_vm._v(" "),
_c("div",{staticClass:"line my-2 py-1"},[
_c("span",{staticClass:"mx-auto"})]),

_vm._v(" "),
_c("p",{staticClass:"text-small mb-2"},[
_vm._v(
"It was refreshing to learn from an expert who so generously shares insights and provides bespoke recommendations on the latest tools to improve your online presence.")]),


_vm._v(" "),
_c("p",{staticClass:"mb-0"},[
_c(
"a",
{
staticClass:"text-white",
attrs:{
href:"https://uk.linkedin.com/in/czarina-sheikh-mathew",
target:"_blank",
"aria-label":"LinkedIn",
rel:"noreferrer"}},


[
_c("i",{staticClass:"fa fa-linkedin-square mr-2"}),
_vm._v(" @czarina-sheikh-mathew")])])])])]);







}];

_render._withStripped=true;



/***/}}]);

}());
