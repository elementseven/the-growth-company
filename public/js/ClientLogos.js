(function () {
'use strict';

(self["webpackChunk"]=self["webpackChunk"]||[]).push([["ClientLogos"],{

/***/"./resources/js/components/ClientLogos.vue":



function resourcesJsComponentsClientLogosVue(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"default":function _default(){return __WEBPACK_DEFAULT_EXPORT__;}
/* harmony export */});
/* harmony import */var _ClientLogos_vue_vue_type_template_id_d03c50fc___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! ./ClientLogos.vue?vue&type=template&id=d03c50fc& */"./resources/js/components/ClientLogos.vue?vue&type=template&id=d03c50fc&");
/* harmony import */var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__=__webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */"./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script={}


/* normalize component */;

var component=(0, _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
script,
_ClientLogos_vue_vue_type_template_id_d03c50fc___WEBPACK_IMPORTED_MODULE_0__.render,
_ClientLogos_vue_vue_type_template_id_d03c50fc___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
false,
null,
null,
null);
component.options.__file="resources/js/components/ClientLogos.vue";
/* harmony default export */var __WEBPACK_DEFAULT_EXPORT__=component.exports;

/***/},

/***/"./resources/js/components/ClientLogos.vue?vue&type=template&id=d03c50fc&":



function resourcesJsComponentsClientLogosVueVueTypeTemplateIdD03c50fc(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ClientLogos_vue_vue_type_template_id_d03c50fc___WEBPACK_IMPORTED_MODULE_0__.render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* reexport safe */_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ClientLogos_vue_vue_type_template_id_d03c50fc___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns);}
/* harmony export */});
/* harmony import */var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ClientLogos_vue_vue_type_template_id_d03c50fc___WEBPACK_IMPORTED_MODULE_0__=__webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ClientLogos.vue?vue&type=template&id=d03c50fc& */"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/ClientLogos.vue?vue&type=template&id=d03c50fc&");


/***/},

/***/"./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/ClientLogos.vue?vue&type=template&id=d03c50fc&":



function node_modulesVueLoaderLibLoadersTemplateLoaderJsVueLoaderOptionsNode_modulesVueLoaderLibIndexJsVueLoaderOptionsResourcesJsComponentsClientLogosVueVueTypeTemplateIdD03c50fc(__unused_webpack_module,__webpack_exports__,__webpack_require__){

__webpack_require__.r(__webpack_exports__);
/* harmony export */__webpack_require__.d(__webpack_exports__,{
/* harmony export */"render":function render(){return(/* binding */_render);},
/* harmony export */"staticRenderFns":function staticRenderFns(){return(/* binding */_staticRenderFns);}
/* harmony export */});
var _render=function _render(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _vm._m(0);
};
var _staticRenderFns=[
function(){
var _vm=this;
var _h=_vm.$createElement;
var _c=_vm._self._c||_h;
return _c("div",{staticClass:"container"},[
_c("div",{staticClass:"row justify-content-center"},[
_c("div",{staticClass:"col-4 col-lg px-4 mb-5 mob-mb-3"},[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/logos/clients/Brand-Elevation.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/logos/clients/Brand-Elevation.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy mx-auto w-100 h-auto",
attrs:{
src:"/img/logos/clients/Brand-Elevation.png",
type:"image/png",
alt:"Brand Elevation logo",
width:"200",
height:"110"}})])]),




_vm._v(" "),
_c("div",{staticClass:"col-4 col-lg px-4 mb-5 mob-mb-3"},[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/logos/clients/DigiTally.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/logos/clients/DigiTally.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy mx-auto w-100 h-auto",
attrs:{
src:"/img/logos/clients/DigiTally.png",
type:"image/png",
alt:"DigiTally logo",
width:"200",
height:"110"}})])]),




_vm._v(" "),
_c("div",{staticClass:"col-4 col-lg px-4 mb-5 mob-mb-3"},[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/logos/clients/elite.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/logos/clients/elite.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy mx-auto w-100 h-auto",
attrs:{
src:"/img/logos/clients/elite.png",
type:"image/png",
alt:"elite logo",
width:"200",
height:"110"}})])]),




_vm._v(" "),
_c("div",{staticClass:"col-4 col-lg px-4 mb-5 mob-mb-3"},[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/logos/clients/gibsonbros.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/logos/clients/gibsonbros.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy mx-auto w-100 h-auto",
attrs:{
src:"/img/logos/clients/gibsonbros.png",
type:"image/png",
alt:"gibsonbros logo",
width:"200",
height:"110"}})])]),




_vm._v(" "),
_c("div",{staticClass:"col-4 col-lg px-4 mb-5 mob-mb-3"},[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/logos/clients/hutchinsonengineering.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/logos/clients/hutchinsonengineering.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy mx-auto w-100 h-auto",
attrs:{
src:"/img/logos/clients/hutchinsonengineering.png",
type:"image/png",
alt:"hutchinsonengineering logo",
width:"200",
height:"110"}})])]),




_vm._v(" "),
_c(
"div",
{staticClass:"col-4 col-lg px-4 mb-5 mob-mb-3 d-lg-none"},
[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/logos/clients/joseph-murphy.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/logos/clients/joseph-murphy.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy mx-auto w-100 h-auto",
attrs:{
src:"/img/logos/clients/joseph-murphy.png",
type:"image/png",
alt:"joseph murphy logo",
width:"200",
height:"110"}})])])]),






_vm._v(" "),
_c("div",{staticClass:"row justify-content-center"},[
_c(
"div",
{staticClass:"col-4 col-lg px-4 mb-5 mob-mb-3 d-none d-lg-block"},
[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/logos/clients/joseph-murphy.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/logos/clients/joseph-murphy.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy mx-auto w-100 h-auto",
attrs:{
src:"/img/logos/clients/joseph-murphy.png",
type:"image/png",
alt:"joseph murphy logo",
width:"200",
height:"110"}})])]),





_vm._v(" "),
_c("div",{staticClass:"col-4 col-lg px-4 mb-5 mob-mb-3"},[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/logos/clients/lough-tec.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/logos/clients/lough-tec.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy mx-auto w-100 h-auto",
attrs:{
src:"/img/logos/clients/lough-tec.png",
type:"image/png",
alt:"lough-tec logo",
width:"200",
height:"110"}})])]),




_vm._v(" "),
_c("div",{staticClass:"col-4 col-lg px-4 mb-5 mob-mb-3"},[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/logos/clients/pfp-wealth.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/logos/clients/pfp-wealth.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy mx-auto w-100 h-auto",
attrs:{
src:"/img/logos/clients/pfp-wealth.png",
type:"image/png",
alt:"pfp-wealth logo",
width:"200",
height:"110"}})])]),




_vm._v(" "),
_c("div",{staticClass:"col-4 col-lg px-4 mb-5 mob-mb-3"},[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/logos/clients/pocket-box.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/logos/clients/pocket-box.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy mx-auto w-100 h-auto",
attrs:{
src:"/img/logos/clients/pocket-box.png",
type:"image/png",
alt:"pocket-box logo",
width:"200",
height:"110"}})])]),




_vm._v(" "),
_c("div",{staticClass:"col-4 col-lg px-4 mb-5 mob-mb-3"},[
_c("picture",[
_c("source",{
attrs:{
srcset:"/img/logos/clients/thinkpeople.webp",
type:"image/webp"}}),


_vm._v(" "),
_c("source",{
attrs:{
srcset:"/img/logos/clients/thinkpeople.png",
type:"image/png"}}),


_vm._v(" "),
_c("img",{
staticClass:"lazy mx-auto w-100 h-auto",
attrs:{
src:"/img/logos/clients/thinkpeople.png",
type:"image/png",
alt:"thinkpeople logo",
width:"200",
height:"110"}})])])])]);






}];

_render._withStripped=true;



/***/}}]);

}());
